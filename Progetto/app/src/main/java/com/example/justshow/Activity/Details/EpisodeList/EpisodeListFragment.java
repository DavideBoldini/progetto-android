package com.example.justshow.Activity.Details.EpisodeList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.R;
import com.example.justshow.RecyclerView.EpisodeList.EpisodeListAdapter;
import com.example.justshow.Utilities.ApiResponse;
import com.example.justshow.Utilities.TvSeriesApi;

import info.movito.themoviedbapi.model.tv.TvSeason;

public class EpisodeListFragment extends Fragment {

    private RecyclerView recyclerView;
    private EpisodeListAdapter episodeListAdapter;
    private final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
    private int seasonInt;
    private String series;
    private TvSeason season;

    public EpisodeListFragment() {
    }

    public EpisodeListFragment(String series, int season) {
        this.series = series;
        this.seasonInt = season;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            seasonInt = savedInstanceState.getInt("SEASON_INT");
            series = savedInstanceState.getString("SERIES");
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt("SEASON_INT", seasonInt);
        outState.putString("SERIES", series);
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.episode_list_page, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();
        if (activity != null){
            setRecyclerView();
        }else{
            Log.e("EpisodeListFrag","Activity is null");
        }
    }

    private void setRecyclerView(){
        recyclerView = getView().findViewById(R.id.episodeListRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(llm);

        new TvSeriesApi(new ApiResponse() {
            @Override
            public void processFinish(Object output) {
                season = (TvSeason) output;
                episodeListAdapter = new EpisodeListAdapter(season.getEpisodes());
                recyclerView.setAdapter(episodeListAdapter);
            }
        }).execute("Get Season Serie By Num", series, String.valueOf(seasonInt));

    }
}
