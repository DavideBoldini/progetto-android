package com.example.justshow.RecyclerView.MoviesList;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.Activity.Details.IntentDetailsCreator;
import com.example.justshow.Activity.Home.Profile.ProfileViewModel;
import com.example.justshow.Items.ProfileItem;
import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;

public class MoviesPopularListAdapter extends RecyclerView.Adapter<MoviesPopularViewHolder>{

    private final List<MovieDb> moviesList;
    private final Activity activity;
    ShineButton favorite;

    public MoviesPopularListAdapter(final List<MovieDb> moviesList, final Activity activity){
        this.moviesList = moviesList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MoviesPopularViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movies_popular_row,parent,false);
        favorite = layoutView.findViewById(R.id.favoritePopularMoviesFilterButton);
        return new MoviesPopularViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesPopularViewHolder holder, int position) {
        MovieDb currentPopularMovieItem = moviesList.get(position);

        holder.loadImage("https://image.tmdb.org/t/p/original" + currentPopularMovieItem.getPosterPath(), activity);
        holder.titlePopularMoviesRowTextView.setText(currentPopularMovieItem.getTitle());

        if (currentPopularMovieItem.getOverview().isEmpty()){
            holder.plotPopularMoviesRowTextView.setText("Nessuna trama disponibile");
        }else{
            holder.plotPopularMoviesRowTextView.setText(currentPopularMovieItem.getOverview());
        }

        holder.popularityPopularMoviesRowTextView.setText(String.valueOf(currentPopularMovieItem.getPopularity()));

        holder.itemView.setOnClickListener(v -> {
            IntentDetailsCreator intentCreator = new IntentDetailsCreator();
            Intent intent = intentCreator.createIntentMovies(activity, "Popular", currentPopularMovieItem.getId());
            activity.startActivity(intent);
        });

        ProfileViewModel profileViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(ProfileViewModel.class);
        ProfileItem activeProfile = ((JustShow) activity.getApplication()).getActiveProfile();

        favorite.setChecked(activeProfile != null && activeProfile.getFavoriteMovieList().contains(currentPopularMovieItem));

        favorite.setOnClickListener(v -> {
            if (activeProfile != null) {
                List<MovieDb> newList = activeProfile.getFavoriteMovieList();
                if (newList.contains(currentPopularMovieItem)){
                    newList.remove(currentPopularMovieItem);
                }else {
                    newList.add(currentPopularMovieItem);
                }
                activeProfile.setFavoriteMovieList(newList);
                profileViewModel.updateFavoriteList(activeProfile.getFavoriteMovieList(),activeProfile.getFavoriteTvSeriesList(),activeProfile.getId());
            }else {
                favorite.setChecked(false);
                Toast.makeText(activity,"Necessario effettuare il login", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
