package com.example.justshow.Activity.Home.Profile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.justshow.Activity.Details.DetailsActivity;
import com.example.justshow.Activity.Home.MainActivity;
import com.example.justshow.Activity.Register.RegisterActivity;
import com.example.justshow.Items.ProfileItem;
import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.example.justshow.Utilities.ApiResponse;
import com.example.justshow.Utilities.MoviesApi;
import com.example.justshow.Utilities.TvSeriesApi;
import com.example.justshow.Utilities.Utilities;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.imageview.ShapeableImageView;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.tv.TvSeries;

public class ProfileFragment extends Fragment {

    ShapeableImageView profileImage;
    TextView nameSurnameTextView;
    TextView birthDateTextView;
    TextView addressTextView;
    TextView cityTextView;
    TextView countryTextView;
    TextView emailTextView;
    BottomNavigationView bottomNavigationView;

    LinearLayoutCompat horizontalFavoriteTvCardLayout;
    LinearLayoutCompat horizontalFavoriteMovieLayout;
    LinearLayoutCompat horizontalWatchlistLayout;

    int favTvSize;
    int favMovieSize;
    int watchlistSize;

    ProfileItem activeProfile;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activeProfile = ((JustShow) getActivity().getApplication()).getActiveProfile();
    }

    @Override
    public void onPause() {
        super.onPause();
        favTvSize = activeProfile.getFavoriteTvSeriesList().size();
        favMovieSize = activeProfile.getFavoriteMovieList().size();
        watchlistSize = activeProfile.getWatchlist().size();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (favTvSize != activeProfile.getFavoriteTvSeriesList().size()){
            fillHorizontalTvSeriesFavorite(horizontalFavoriteTvCardLayout,getLayoutInflater(),activeProfile.getFavoriteTvSeriesList());
        }
        if (favMovieSize != activeProfile.getFavoriteMovieList().size()){
            fillHorizontalMoviesFavorite(horizontalFavoriteMovieLayout,getLayoutInflater(),activeProfile.getFavoriteMovieList());
        }
        if (watchlistSize != activeProfile.getWatchlist().size()){
            fillHorizontalWatchlist(activeProfile.getWatchlist(), getLayoutInflater(), horizontalWatchlistLayout);
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_page,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bottomNavigationView = getActivity().findViewById(R.id.bottomAppBar);
        bottomNavigationView.getMenu().findItem(R.id.profile_bottom).setChecked(true);

        profileImage = view.findViewById(R.id.profileImage);
        nameSurnameTextView = view.findViewById(R.id.nameSurnameTextView);
        birthDateTextView = view.findViewById(R.id.birthDateTextView);
        addressTextView = view.findViewById(R.id.addressTextView);
        cityTextView = view.findViewById(R.id.cityTextView);
        countryTextView = view.findViewById(R.id.countryTextView);
        emailTextView = view.findViewById(R.id.emailTextView);

        horizontalFavoriteTvCardLayout = view.findViewById(R.id.horizontalFavoriteTvCardLayout);
        horizontalFavoriteMovieLayout = view.findViewById(R.id.horizontalFavoriteFilmCardLayout);
        horizontalWatchlistLayout = view.findViewById(R.id.horizontalWatchlistCardLayout);

        Button showGmaps = view.findViewById(R.id.showGmapsProfilePage);
        Button editProfile = view.findViewById(R.id.editProfileButton);
        Button logout = view.findViewById(R.id.logoutButtonProfile);
        Button cinema = view.findViewById(R.id.showCinemaProfilePage);

        String profileImagePath = activeProfile.getFotoProfilo();

        if (profileImagePath.contains("ic_")){
            Drawable drawable = ContextCompat.getDrawable(getActivity(),getActivity().getResources().getIdentifier(profileImagePath,"drawable",getActivity().getPackageName()));
            profileImage.setImageDrawable(drawable);
        }else {
            Bitmap bitmap = Utilities.getImageBitmap(getActivity(), Uri.parse(profileImagePath));
            if (bitmap != null){
                profileImage.setImageBitmap(bitmap);
            }
        }

        nameSurnameTextView.setText(activeProfile.getNome() + " " + activeProfile.getCognome());
        birthDateTextView.setText(activeProfile.getDataNascita());
        addressTextView.setText(activeProfile.getIndirizzo());
        cityTextView.setText(activeProfile.getCittà());
        countryTextView.setText(activeProfile.getNazione());
        emailTextView.setText(activeProfile.getEmail());

        fillHorizontalTvSeriesFavorite(horizontalFavoriteTvCardLayout,getLayoutInflater(),activeProfile.getFavoriteTvSeriesList());
        fillHorizontalMoviesFavorite(horizontalFavoriteMovieLayout,getLayoutInflater(),activeProfile.getFavoriteMovieList());
        fillHorizontalWatchlist(activeProfile.getWatchlist(), getLayoutInflater(), horizontalWatchlistLayout);

        logout.setOnClickListener(v -> {
            ((JustShow) getActivity().getApplication()).setUserLogged(false);
            ((JustShow) getActivity().getApplication()).setActiveProfile(null);

            Toast.makeText(getContext(),"Logout eseguito con successo", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            getContext().startActivity(intent);
        });

        showGmaps.setOnClickListener(v -> new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String str_location = Objects.requireNonNull(activeProfile.getIndirizzo());
                String city = Objects.requireNonNull(activeProfile.getCittà());
                String country = activeProfile.getNazione();
                if (!str_location.equals("") && !city.equals("") && country != null){
                    Uri map = Uri.parse("geo:0,0?q=" + str_location + "," + city + "," + country);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, map);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }else {
                    Toast.makeText(getContext(),"Località non valida", Toast.LENGTH_SHORT).show();
                }
            }
        },300));

        cinema.setOnClickListener(v -> new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String str_location = Objects.requireNonNull(activeProfile.getIndirizzo());
                String city = Objects.requireNonNull(activeProfile.getCittà());
                String country = activeProfile.getNazione();

                String totAddress = str_location + ", " + city + ", " + country;

                LatLng res = Utilities.getLocationFromAddress(totAddress, getActivity());

                if (!str_location.equals("") && !city.equals("") && country != null){
                    Uri cinemaUri = Uri.parse("geo:" + res.latitude + "," + res.longitude + "?q=cinema");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, cinemaUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }else {
                    Toast.makeText(getContext(),"Località non valida", Toast.LENGTH_SHORT).show();
                }
            }
        }, 300));

        editProfile.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), RegisterActivity.class);
            Bundle extra = new Bundle();
            extra.putString("Type","Edit Profile");
            intent.putExtras(extra);
            startActivity(intent);
        });

    }

    private void fillHorizontalTvSeriesFavorite(LinearLayoutCompat layout, LayoutInflater inflater, List<TvSeries> tvList){
        layout.removeAllViews();
        for(TvSeries series : tvList){
            View card = inflater.inflate(R.layout.card_layout, layout, false);

            TextView textView = card.findViewById(R.id.title);
            textView.setText(series.getName());

            ImageView imageView = card.findViewById(R.id.thumbnail);

            Picasso.get()
                    .load("https://image.tmdb.org/t/p/w185" + series.getPosterPath())
                    .resize(150,200)
                    .centerCrop()
                    .into(imageView);

            TextView rating = card.findViewById(R.id.rating);
            rating.setText(String.format("%.1f", series.getVoteAverage()));

            card.setOnClickListener(v -> {
                Intent intent = new Intent(getContext(), DetailsActivity.class);

                new TvSeriesApi(new ApiResponse() {
                    @Override
                    public void processFinish(Object output) {
                        TvSeries tv = (TvSeries) output;
                        Bundle extras = new Bundle();
                        extras.putSerializable("Tv", tv);
                        extras.putString("Type", "");
                        intent.putExtras(extras);

                        getContext().startActivity(intent);
                    }
                }).execute("Get dettagli Serie", String.valueOf(series.getId()));
            });

            layout.addView(card);
        }
    }

    private void fillHorizontalMoviesFavorite(LinearLayoutCompat layout, LayoutInflater inflater, List<MovieDb> movieList){
        layout.removeAllViews();
        for(MovieDb movie : movieList){
            View card = inflater.inflate(R.layout.card_layout, layout, false);

            TextView textView = card.findViewById(R.id.title);
            textView.setText(movie.getTitle());

            ImageView imageView = card.findViewById(R.id.thumbnail);

            Picasso.get()
                    .load("https://image.tmdb.org/t/p/w185" + movie.getPosterPath())
                    .resize(150,200)
                    .centerCrop()
                    .into(imageView);

            TextView rating = card.findViewById(R.id.rating);
            rating.setText(String.format("%.1f", movie.getVoteAverage()));

            card.setOnClickListener(v -> {
                Intent intent = new Intent(getContext(), DetailsActivity.class);
                new MoviesApi(new ApiResponse() {
                    @Override
                    public void processFinish(Object output) {
                        MovieDb resMovie = (MovieDb) output;
                        Bundle extras = new Bundle();
                        extras.putSerializable("Movie", resMovie);
                        extras.putString("Type", "Film");
                        intent.putExtras(extras);

                        getContext().startActivity(intent);
                    }
                }).execute("Get dettagli Film by Title", movie.getTitle());
            });

            layout.addView(card);
        }
    }


    private void fillHorizontalWatchlist(HashMap<String,String> watchlistProfile, LayoutInflater inflater, LinearLayoutCompat layout){
        layout.removeAllViews();
        for(Map.Entry<String,String> elem : watchlistProfile.entrySet()){

            View card = inflater.inflate(R.layout.card_layout, layout, false);
            TextView textView = card.findViewById(R.id.title);
            ImageView imageView = card.findViewById(R.id.thumbnail);
            TextView rating = card.findViewById(R.id.rating);

            if (elem.getValue().equals("Tv")){
                new TvSeriesApi(new ApiResponse() {
                   @Override
                   public void processFinish(Object output) {
                       TvSeries tv = (TvSeries) output;
                       textView.setText(tv.getName());
                       Glide.with(getContext())
                               .load("https://image.tmdb.org/t/p/w342" + tv.getPosterPath())
                               .centerCrop()
                               .skipMemoryCache(true)
                               .diskCacheStrategy(DiskCacheStrategy.NONE)
                               .into(imageView);
                       rating.setText(String.format("%.1f", tv.getVoteAverage()));

                       card.setOnClickListener(v -> {
                           Intent intent = new Intent(getContext(), DetailsActivity.class);
                           Bundle extras = new Bundle();
                           extras.putSerializable("Tv", tv);
                           extras.putString("Type","Serie");
                           intent.putExtras(extras);
                           getContext().startActivity(intent);
                       });
                   }
               }).execute("Get dettagli Serie", elem.getKey());
            }else {
                new MoviesApi(new ApiResponse() {
                    @Override
                    public void processFinish(Object output) {
                        MovieDb movie = (MovieDb) output;
                        textView.setText(movie.getTitle());
                        Glide.with(getContext())
                                .load("https://image.tmdb.org/t/p/w342" + movie.getPosterPath())
                                .centerCrop()
                                .skipMemoryCache(true)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(imageView);
                        rating.setText(String.format("%.1f", movie.getVoteAverage()));

                        card.setOnClickListener(v -> {
                            Intent intent = new Intent(getContext(), DetailsActivity.class);
                            Bundle extras = new Bundle();
                            extras.putSerializable("Movie", movie);
                            extras.putString("Type", "Film");
                            intent.putExtras(extras);
                            getContext().startActivity(intent);
                        });
                    }
                }).execute("Get dettagli Film", elem.getKey());
            }
            layout.addView(card);
        }
    }
}
