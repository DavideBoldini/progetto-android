package com.example.justshow.Items;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.HashMap;
import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.tv.TvSeries;

@Entity(tableName = "user")
public class ProfileItem {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    private int id;

    @ColumnInfo(name = "fotoProfilo")
    private String fotoProfilo;

    @ColumnInfo(name ="nome")
    private String nome;

    @ColumnInfo(name = "cognome")
    private String cognome;

    @ColumnInfo(name = "dataNascita")
    private String dataNascita;

    @ColumnInfo(name = "indirizzo")
    private String indirizzo;

    @ColumnInfo(name = "città")
    private String città;

    @ColumnInfo(name = "nazione")
    private String nazione;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "password")
    private String password;

    @ColumnInfo(name = "favoriteFilmList")
    private List<MovieDb> favoriteMovieList;

    @ColumnInfo(name = "favoriteTvSeriesList")
    private List<TvSeries> favoriteTvSeriesList;

    @ColumnInfo(name = "watchlist")
    private HashMap<String,String> watchlist;


    public ProfileItem(final String fotoProfilo, final String nome, final String cognome, final String dataNascita, final String indirizzo, final String città,
                       final String nazione, final String email, final String password, final List<MovieDb> favoriteMovieList, final List<TvSeries> favoriteTvSeriesList,
                       final HashMap<String,String> watchlist){
        this.fotoProfilo = fotoProfilo;
        this.nome = nome;
        this.cognome = cognome;
        this.dataNascita = dataNascita;
        this.indirizzo = indirizzo;
        this.città = città;
        this.nazione = nazione;
        this.email = email;
        this.password = password;
        this.favoriteMovieList = favoriteMovieList;
        this.favoriteTvSeriesList = favoriteTvSeriesList;
        this.watchlist = watchlist;
    }


    public int getId() {return id;}

    public String getFotoProfilo() {
        return fotoProfilo;
    }

    public String getNome() {
        return nome;
    }

    public String getCognome() {
        return cognome;
    }

    public String getDataNascita() {
        return dataNascita;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public String getCittà() {
        return città;
    }

    public String getNazione() {
        return nazione;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public List<MovieDb> getFavoriteMovieList() {
        return favoriteMovieList;
    }

    public List<TvSeries> getFavoriteTvSeriesList() {
        return favoriteTvSeriesList;
    }

    public HashMap<String,String> getWatchlist() {
        return this.watchlist;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFotoProfilo(String fotoProfilo) {
        this.fotoProfilo = fotoProfilo;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public void setDataNascita(String dataNascita) {
        this.dataNascita = dataNascita;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public void setCittà(String città) {
        this.città = città;
    }

    public void setNazione(String nazione) {
        this.nazione = nazione;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFavoriteMovieList(List<MovieDb> favoriteMovieList) {
        this.favoriteMovieList = favoriteMovieList;
    }

    public void setFavoriteTvSeriesList(List<TvSeries> favoriteTvSeriesList) {
        this.favoriteTvSeriesList = favoriteTvSeriesList;
    }

    public void setWatchlist(HashMap<String,String> watchlist){
        this.watchlist = watchlist;
    }
}
