package com.example.justshow.Activity.Details;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.justshow.Activity.Home.MainActivity;
import com.example.justshow.Activity.Home.Profile.ProfileFragment;
import com.example.justshow.Activity.Home.Profile.WelcomeActivity;
import com.example.justshow.Activity.Search.SearchFragment;
import com.example.justshow.Items.ProfileItem;
import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.example.justshow.Utilities.Utilities;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class DetailsActivity extends AppCompatActivity implements NavigationBarView.OnItemSelectedListener {

    BottomNavigationView bottom;
    boolean userLogged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity_layout);

        userLogged = ((JustShow) this.getApplication()).getUserLogged();

        bottom = findViewById(R.id.bottomAppBar);
        bottom.setOnItemSelectedListener(this);
        bottom.getMenu().findItem(R.id.home_bottom).setCheckable(false);
        bottom.getMenu().findItem(R.id.home_bottom).setCheckable(false);

        if (userLogged){
            ProfileItem activeProfile = ((JustShow) getApplication()).getActiveProfile();
            Menu menu = bottom.getMenu();
            menu.findItem(R.id.profile_bottom).setTitle(activeProfile.getNome());
        }

        if (savedInstanceState == null){
            Utilities.insertFragmentDetails(this,new DetailsFragment(),"Details Fragment");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        uncheckAllItems();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        Fragment fragment = null;
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        String tag = "";

        switch (id){
            case R.id.home_bottom:
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(intent);
                return true;
            case R.id.search_bottom:
                fragment = new SearchFragment();
                tag = "SearchFragment";
                break;
            case R.id.profile_bottom:
                if (!userLogged){
                    Intent intentWelcome = new Intent(this, WelcomeActivity.class);
                    this.startActivity(intentWelcome);
                    return true;
                }else {
                    fragment = new ProfileFragment();
                    tag = "Profile Fragment";
                    break;
                }
        }
        fragmentTransaction.replace(R.id.detailsFragmentContainerView, fragment, tag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        return true;
    }

    public void uncheckAllItems(){
        bottom.getMenu().setGroupCheckable(0,true,false);
        Menu menu = bottom.getMenu();
        for (int i = 0; i< menu.size(); i++){
            menu.getItem(i).setChecked(false);
        }
        menu.setGroupCheckable(0,true,true);
    }
}
