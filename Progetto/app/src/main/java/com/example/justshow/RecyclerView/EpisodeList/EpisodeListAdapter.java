package com.example.justshow.RecyclerView.EpisodeList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import info.movito.themoviedbapi.model.tv.TvEpisode;

public class EpisodeListAdapter extends RecyclerView.Adapter<EpisodeListViewHolder> {

    private final List<TvEpisode> episodeItemList;

    public EpisodeListAdapter(List<TvEpisode> episodeItemList) {
        this.episodeItemList = episodeItemList;
    }

    @NonNull
    @Override
    public EpisodeListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.episode_row,parent,false);
        return new EpisodeListViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull EpisodeListViewHolder holder, int position) {
        TvEpisode currentEpisodeItem = episodeItemList.get(position);

        holder.titleTextView.setText(currentEpisodeItem.getName());
        holder.dateTextView.setText(currentEpisodeItem.getAirDate());

        if (currentEpisodeItem.getOverview() == null || currentEpisodeItem.getOverview().equals("")){
            holder.plotTextView.setText("Trama non disponibile");
        }else{
            holder.plotTextView.setText(currentEpisodeItem.getOverview());
        }

        if (currentEpisodeItem.getVoteAverage() == 0.0){
            holder.ratingTextView.setText("Nessun voto");
        }else{
            holder.ratingTextView.setText(String.format("%.1f", currentEpisodeItem.getVoteAverage()));
        }


        if (currentEpisodeItem.getStillPath() != null){
            Picasso.get()
                    .load("https://image.tmdb.org/t/p/original" + currentEpisodeItem.getStillPath())
                    .fit()
                    .into(holder.imageEpisodeView);
        }else {
            holder.imageEpisodeView.setScaleX((float) 0.6);
            holder.imageEpisodeView.setScaleY((float) 0.6);
        }

    }

    @Override
    public int getItemCount() {
        return episodeItemList.size();
    }
}
