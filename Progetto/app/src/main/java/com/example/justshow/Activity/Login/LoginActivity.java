package com.example.justshow.Activity.Login;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.justshow.R;
import com.example.justshow.Utilities.Utilities;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity_layout);

        if (savedInstanceState == null){
            Utilities.insertFragmentLogin(this,new LoginFragment(),"LoginFragment");
        }

    }
}
