package com.example.justshow.Activity.Home.Profile;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.justshow.Activity.Login.LoginActivity;
import com.example.justshow.Activity.Register.RegisterActivity;
import com.example.justshow.R;

public class WelcomeFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.welcome_page,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button entra = view.findViewById(R.id.welcomeLoginButton);

        entra.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            getContext().startActivity(intent);
        });

        entra.setBackgroundColor(Color.parseColor("#B3BB86FC"));
        entra.setTextColor(Color.parseColor("#e5e5e5"));

        Button register = view.findViewById(R.id.welcomeRegisterButton);
        register.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), RegisterActivity.class);
            Bundle extra = new Bundle();
            extra.putString("Type","New Profile");
            intent.putExtras(extra);
            getContext().startActivity(intent);
        });

        register.setBackgroundColor(Color.parseColor("#B3333333"));
        
        VideoView backgroundVideo = view.findViewById(R.id.videoBackground);

        backgroundVideo.setVideoURI(Uri.parse("android.resource://" + getContext().getPackageName() + "/" + R.raw.background_video2));
        backgroundVideo.setOnPreparedListener(mp -> {
            mp.setLooping(true);
            backgroundVideo.start();
        });

    }
}
