package com.example.justshow.RecyclerView.News;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.R;
import com.google.android.material.imageview.ShapeableImageView;

public class NewsListViewHolder extends RecyclerView.ViewHolder {

    ShapeableImageView thumbnail;
    TextView title;
    TextView description;
    TextView source;

    public NewsListViewHolder(@NonNull View itemView){
        super(itemView);

        thumbnail = itemView.findViewById(R.id.thumbnailNewsRow);
        title = itemView.findViewById(R.id.titleNewsRow);
        description = itemView.findViewById(R.id.descriptionNewsRow);
        source = itemView.findViewById(R.id.sourceNewsRow);
    }
}
