package com.example.justshow.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.justshow.Items.ProfileItem;

import java.util.HashMap;
import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.tv.TvSeries;

@Dao
public interface ProfileDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addProfileItem(ProfileItem profileItem);

    @Transaction
    @Query("SELECT * from user WHERE email = :email")
    LiveData<List<ProfileItem>> getProfile(String email);

    @Transaction
    @Query("SELECT * from user WHERE email = :email and password = :password")
    LiveData<List<ProfileItem>> checkLogin(String email,String password);

    @Transaction
    @Query("SELECT * from user")
    LiveData<List<ProfileItem>> getAllProfile();

    @Transaction
    @Query("Update user set favoriteFilmList = :filmList, favoriteTvSeriesList = :tvList where ID = :id")
    void updateFavoriteList(List<MovieDb> filmList, List<TvSeries> tvList, int id);

    @Transaction
    @Query("Update user set watchlist = :watchlist where ID = :id")
    void updateWatchlist(HashMap<String,String> watchlist, int id);

    @Transaction
    @Query("Update user set fotoProfilo = :thumbnail, nome = :name, cognome = :surname, dataNascita = :birthdate, indirizzo = :address, città = :city, nazione = :country, email = :email, password = :password")
    void updateProfileDetails(String thumbnail, String name, String surname, String birthdate, String address, String city, String country, String email, String password);
}
