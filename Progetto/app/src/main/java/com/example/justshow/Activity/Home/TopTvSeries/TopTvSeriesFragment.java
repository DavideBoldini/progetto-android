package com.example.justshow.Activity.Home.TopTvSeries;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.R;
import com.example.justshow.RecyclerView.SeriesList.SeriesTopListAdapter;
import com.example.justshow.Utilities.TvSeriesUtilities;

import java.util.List;

import info.movito.themoviedbapi.model.tv.TvSeries;

public class TopTvSeriesFragment extends Fragment {

    private RecyclerView recyclerView;
    private SeriesTopListAdapter seriesTopListAdapter;
    LinearLayoutManager llm = new LinearLayoutManager(getActivity());

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.top_tvseries,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Activity activity = getActivity();
        if (activity != null){
            setRecyclerView(activity);
        }else{
            Log.e("TopTvSeries.java","Activity is null");
        }
    }

    private void setRecyclerView(final Activity activity){
        recyclerView = getView().findViewById(R.id.allTopTvSeriesListRecyclerView);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(llm);
        List<TvSeries> list = TvSeriesUtilities.getTopTv();
        seriesTopListAdapter = new SeriesTopListAdapter(list, activity);
        recyclerView.setAdapter(seriesTopListAdapter);
    }
}
