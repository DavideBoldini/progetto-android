package com.example.justshow.Activity.Details;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.justshow.R;
import com.example.justshow.Utilities.ApiResponse;
import com.example.justshow.Utilities.MoviesApi;
import com.example.justshow.Utilities.PersonApi;
import com.example.justshow.Utilities.TvSeriesApi;
import com.squareup.picasso.Picasso;

import java.util.List;

import info.movito.themoviedbapi.model.Artwork;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.people.PersonCredits;
import info.movito.themoviedbapi.model.people.PersonCrew;
import info.movito.themoviedbapi.model.people.PersonPeople;
import info.movito.themoviedbapi.model.tv.TvSeries;

public class DirectorFragment extends Fragment {

    Bundle extras;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extras = this.getArguments();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((DetailsActivity)getActivity()).uncheckAllItems();
        return inflater.inflate(R.layout.person_page, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        LottieAnimationView thumbnail = view.findViewById(R.id.thumbnailPersonDetails);
        TextView namePersonTextView = view.findViewById(R.id.nameDetails);
        TextView biographyTextView = view.findViewById(R.id.biographyDetails);
        TextView creditsPersonDetailsTextView = view.findViewById(R.id.creditsPersonDetailsTextView);
        LottieAnimationView castAnimationView = view.findViewById(R.id.castAnimationView);

        LinearLayoutCompat horizontalGalleryPersonLayout = view.findViewById(R.id.horizontalGalleryPersonLayout);

        LinearLayoutCompat horizontalCastPersonLayout = view.findViewById(R.id.horizontalCastDetailsLayout);

        TextView genrePersonTextView = view.findViewById(R.id.genrePersonLabelDetailsTextView);
        TextView birthDateTextView = view.findViewById(R.id.birthDatePersonDetailsTextView);
        TextView birthPlaceTextView = view.findViewById(R.id.placeBirthDetailsTextView);

        TextView deathDatePersonDetailsTextView = view.findViewById(R.id.deathDatePersonDetailsTextView);
        View deathDateDetailsDivider = view.findViewById(R.id.deathDateDetailsDivider);

        PersonCrew person = (PersonCrew) extras.getSerializable("person");

        creditsPersonDetailsTextView.setText("Ha diretto");
        castAnimationView.setAnimation("cameraLottie.json");
        castAnimationView.setRepeatCount(LottieDrawable.INFINITE);

        new PersonApi(new ApiResponse() {
            @Override
            public void processFinish(Object output) {
                PersonPeople detailPerson = (PersonPeople) output;
                fillLayout(detailPerson,thumbnail,namePersonTextView,biographyTextView,genrePersonTextView,birthDateTextView,birthPlaceTextView, deathDatePersonDetailsTextView,
                        deathDateDetailsDivider);
            }
        }).execute("Get Dettagli Attore", String.valueOf(person.getId()));

        new PersonApi(new ApiResponse() {
            @Override
            public void processFinish(Object output) {
                List<Artwork> resImg = (List<Artwork>) output;
                fillGallery(resImg,horizontalGalleryPersonLayout);
            }
        }).execute("Get Immagini Attore", String.valueOf(person.getId()));

        new PersonApi(new ApiResponse() {
            @Override
            public void processFinish(Object output) {
                PersonCredits res = (PersonCredits) output;
                fillCredits(res,horizontalCastPersonLayout);
            }
        }).execute("Get Credits Attore", String.valueOf(person.getId()));

    }

    private void fillCredits(PersonCredits res, LinearLayoutCompat horizontalCastPersonLayout) {
        for (int i = 0; i < res.getCast().size(); i++){

            if (i == 50){
                break;
            }

            View card = LayoutInflater.from(getActivity()).inflate(R.layout.card_layout, horizontalCastPersonLayout, false);

            ImageView thumb = card.findViewById(R.id.thumbnail);
            TextView title = card.findViewById(R.id.title);
            TextView rating = card.findViewById(R.id.rating);

            if (res.getCast().get(i).getMediaType().equals("movie")){
                title.setText(res.getCast().get(i).getMovieTitle());
            }else {
                title.setText(res.getCast().get(i).getSeriesName());
            }

            rating.setText(String.format("%.1f", res.getCast().get(i).getVoteAvg()));


            if (res.getCast().get(i).getPosterPath() != null){
                Glide.with(getContext())
                        .load("https://image.tmdb.org/t/p/original" + res.getCast().get(i).getPosterPath())
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(thumb);
            }

            int index = i;
            card.setOnClickListener(v -> {
                Intent intent = new Intent(getContext(), DetailsActivity.class);

                if (res.getCast().get(index).getMediaType().equals("movie")){
                    new MoviesApi(new ApiResponse() {
                        @Override
                        public void processFinish(Object output) {
                            MovieDb movie = (MovieDb) output;
                            Bundle extras = new Bundle();
                            extras.putSerializable("Movie", movie);
                            extras.putString("Type", "");
                            intent.putExtras(extras);

                            getContext().startActivity(intent);
                        }
                    }).execute("Get dettagli Film", String.valueOf(res.getCast().get(index).getId()));
                }else {
                    new TvSeriesApi(new ApiResponse() {
                        @Override
                        public void processFinish(Object output) {
                            TvSeries serie = (TvSeries) output;
                            Bundle extras = new Bundle();
                            extras.putSerializable("Tv", serie);
                            extras.putString("Type", "");
                            intent.putExtras(extras);

                            getContext().startActivity(intent);
                        }
                    }).execute("Get dettagli Serie", String.valueOf(res.getCast().get(index).getId()));
                }
            });
            horizontalCastPersonLayout.addView(card);
        }
    }

    private void fillGallery(List<Artwork> resImg, LinearLayoutCompat horizontalGalleryPersonLayout) {
        for (Artwork art : resImg){
            ImageView img = new ImageView(this.getContext());
            Picasso.get()
                    .load("https://image.tmdb.org/t/p/original" + art.getFilePath())
                    .resize(700,500)
                    .centerInside()
                    .into(img);
            horizontalGalleryPersonLayout.addView(img);
        }
    }

    private void fillLayout(PersonPeople personIn, LottieAnimationView thumbIn,TextView nameIn, TextView bioIn, TextView genreIn, TextView birthDateIn,
                            TextView birthPlaceIn, TextView deathDateIn, View deathDivider){
        fillImage(personIn,thumbIn);

        nameIn.setText(personIn.getName());
        if (personIn.getBiography() != null && !personIn.getBiography().equals("")){
            bioIn.setText(personIn.getBiography());
        }else{
            bioIn.setText("Nessuna informazione disponibile");
        }

        if (personIn.getGender() == 2){
            genreIn.append(" Uomo");
        }else {
            genreIn.append(" Donna");
        }

        if (personIn.getBirthday() != null && !personIn.getBirthday().equals("")){
            birthDateIn.append(": " + personIn.getBirthday());
            birthPlaceIn.append(": \n" + personIn.getBirthplace());
        }else {
            birthDateIn.append(": Nessuna informazione");
            birthPlaceIn.append(": Nessuna informazione");
        }

        if (personIn.getDeathday() == null || personIn.getDeathday().equals("")){
            deathDateIn.setVisibility(View.GONE);
            deathDivider.setVisibility(View.GONE);
        }else {
            deathDateIn.append(" " + personIn.getDeathday());
        }
    }

    private void fillImage(PersonPeople personIn, LottieAnimationView thumbIn){
        if (personIn.getProfilePath() != null && !personIn.getProfilePath().equals("")){
            Glide.with(getContext())
                    .load("https://image.tmdb.org/t/p/original" + personIn.getProfilePath())
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(thumbIn);
        }

    }
}
