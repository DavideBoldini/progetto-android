package com.example.justshow.Activity.Home.UpcomingMovies;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.R;
import com.example.justshow.RecyclerView.MoviesList.MoviesUpcomingListAdapter;
import com.example.justshow.Utilities.MoviesUtilities;

import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;

public class UpcomingMoviesFragment extends Fragment {

    private RecyclerView recyclerView;
    private MoviesUpcomingListAdapter moviesUpcomingListAdapter;
    LinearLayoutManager llm = new LinearLayoutManager(getActivity());

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.upcoming_movies,container, false);
        View view =  inflater.inflate(R.layout.upcoming_movies, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();

        if (activity != null){
            setRecyclerView(activity);
        }else{
            Log.e("UpcomingTvSeries.java","Activity is null");
        }
    }

    private void setRecyclerView(final Activity activity){
        recyclerView = getView().findViewById(R.id.allUpcomingMoviesListRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(llm);
        List<MovieDb> list = MoviesUtilities.getUpcomingMovies();
        moviesUpcomingListAdapter = new MoviesUpcomingListAdapter(list,activity);
        recyclerView.setAdapter(moviesUpcomingListAdapter);
    }
}
