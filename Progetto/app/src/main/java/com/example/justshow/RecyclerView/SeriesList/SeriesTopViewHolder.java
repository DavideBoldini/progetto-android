package com.example.justshow.RecyclerView.SeriesList;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.justshow.R;
import com.sackcentury.shinebuttonlib.ShineButton;

public class SeriesTopViewHolder extends RecyclerView.ViewHolder{

    ImageView thumbnailTopTvRowImageView;
    ShineButton favoriteTopTvFilterButton;
    TextView titleTopTvRowTextView;
    TextView plotTopTvRowTextView;
    View dividerTopTvRowView;
    TextView ratingTopTvRowTextView;

    public SeriesTopViewHolder(@NonNull View itemView) {
        super(itemView);

        thumbnailTopTvRowImageView = itemView.findViewById(R.id.thumbnailTopSeriesTvRow);
        favoriteTopTvFilterButton = itemView.findViewById(R.id.favoriteTopSeriesTvFilterButton);
        titleTopTvRowTextView = itemView.findViewById(R.id.titleTopSeriesTvRow);
        plotTopTvRowTextView = itemView.findViewById(R.id.plotTopSeriesTvRow);
        dividerTopTvRowView = itemView.findViewById(R.id.plotTopSeriesTvRowDivider);
        ratingTopTvRowTextView = itemView.findViewById(R.id.ratingTopSeriesTvRow);
    }

    public void loadImage(String posterPath, Activity activity){
        Glide.with(activity)
                .load(posterPath)
                .error(R.drawable.ic_baseline_image_not_supported_24)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(thumbnailTopTvRowImageView);
    }
}
