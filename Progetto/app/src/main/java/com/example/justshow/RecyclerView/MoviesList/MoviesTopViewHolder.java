package com.example.justshow.RecyclerView.MoviesList;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.justshow.R;
import com.sackcentury.shinebuttonlib.ShineButton;

public class MoviesTopViewHolder extends RecyclerView.ViewHolder {

    ImageView thumbnailTopMoviesRowImageView;
    ShineButton favoriteTopMoviesFilterButton;
    TextView titleTopMoviesRowTextView;
    TextView plotTopMoviesRowTextView;
    View dividerTopMoviesRowView;
    TextView ratingTopMoviesRowTextView;

    public MoviesTopViewHolder(@NonNull View itemView) {
        super(itemView);

        thumbnailTopMoviesRowImageView = itemView.findViewById(R.id.thumbnailTopMoviesRow);
        favoriteTopMoviesFilterButton = itemView.findViewById(R.id.favoriteTopMoviesFilterButton);
        titleTopMoviesRowTextView = itemView.findViewById(R.id.titleTopMoviesRow);
        plotTopMoviesRowTextView = itemView.findViewById(R.id.plotTopMoviesRow);
        dividerTopMoviesRowView = itemView.findViewById(R.id.plotTopMoviesRowDivider);
        ratingTopMoviesRowTextView = itemView.findViewById(R.id.ratingTopMoviesRow);

    }

    public void loadImage(String posterPath, Activity activity){
        Glide.with(activity)
                .load(posterPath)
                .error(R.drawable.ic_baseline_image_not_supported_24)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(thumbnailTopMoviesRowImageView);
    }
}
