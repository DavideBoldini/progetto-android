package com.example.justshow.Activity.Home.PopularTvSeries;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.R;
import com.example.justshow.RecyclerView.SeriesList.SeriesPopularListAdapter;
import com.example.justshow.Utilities.TvSeriesUtilities;

import java.util.List;

import info.movito.themoviedbapi.model.tv.TvSeries;

public class PopularTvSeriesFragment extends Fragment {
    private RecyclerView recyclerView;
    private SeriesPopularListAdapter seriesPopularListAdapter;
    LinearLayoutManager llm = new LinearLayoutManager(getActivity());

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.popular_tvseries,container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();
        if (activity != null){
            setRecyclerView(activity);
        }else{
            Log.e("PopularTvSeries.java","Activity is null");
        }
    }

    private void setRecyclerView(final Activity activity){
        recyclerView = getView().findViewById(R.id.allPopularTvSeriesListRecyclerView);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(llm);
        List<TvSeries> list = TvSeriesUtilities.getPopTv();
        seriesPopularListAdapter = new SeriesPopularListAdapter(list,activity);
        recyclerView.setAdapter(seriesPopularListAdapter);
    }
}
