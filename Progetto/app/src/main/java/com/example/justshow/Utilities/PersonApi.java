package com.example.justshow.Utilities;

import android.os.AsyncTask;

import java.util.Collections;
import java.util.List;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.TmdbPeople;
import info.movito.themoviedbapi.model.Artwork;
import info.movito.themoviedbapi.model.people.PersonCredits;
import info.movito.themoviedbapi.model.people.PersonPeople;

public class PersonApi extends AsyncTask<String,Void,Object> {

    TmdbApi tmdb;
    public ApiResponse delegate;

    public PersonApi(ApiResponse delegate){this.delegate = delegate; }

    @Override
    protected Object doInBackground(String... strings) {
        tmdb = new TmdbApi("db981d35add49fc538c2afdd56fd00cf");
        switch(strings[0]){
            case "Get Dettagli Attore":
                return this.getPersonDetails(tmdb,strings[1]);
            case "Get Credits Attore":
                return this.getCreditsAttore(tmdb,strings[1]);
            case "Get Immagini Attore":
                return this.getImmaginiAttore(tmdb,strings[1]);
        }
        return Collections.emptyList();
    }

    @Override
    protected void onPostExecute(Object o) {
        if (delegate != null){
            delegate.processFinish(o);
        }
    }

    private List<Artwork> getImmaginiAttore(TmdbApi tmdb, String idIn) {
        int id = Integer.parseInt(idIn);
        TmdbPeople tmdbPeople = tmdb.getPeople();
        List<Artwork> res = tmdbPeople.getPersonImages(id);
        return res;
    }

    private PersonPeople getPersonDetails(TmdbApi tmdb, String idIn){
        int id = Integer.parseInt(idIn);
        TmdbPeople tmdbPeople = tmdb.getPeople();
        PersonPeople res = tmdbPeople.getPersonInfo(id);
        return res;
    }

    private PersonCredits getCreditsAttore(TmdbApi tmdb, String idIn) {
        int id = Integer.parseInt(idIn);
        DetailsTmdbPeopleApi tmdbPeople = new DetailsTmdbPeopleApi(tmdb);
        PersonCredits res = tmdbPeople.getCombinedPersonCredits(id,"it-IT");
        return res;
    }


}
