package com.example.justshow.Activity.Search;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.justshow.R;
import com.example.justshow.Utilities.Utilities;

import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.tv.TvSeries;

public class SearchResultActivity extends AppCompatActivity{

    public static List<MovieDb> totMovieList;
    public static List<TvSeries> totTvList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_result_activity_layout);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (savedInstanceState == null){
            Utilities.insertFragmentResult(this,new SearchResultFragment(extras),"Search Result Fragment");
        }
    }

}
