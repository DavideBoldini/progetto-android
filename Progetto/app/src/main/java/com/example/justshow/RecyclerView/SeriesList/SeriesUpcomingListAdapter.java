package com.example.justshow.RecyclerView.SeriesList;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.Activity.Details.IntentDetailsCreator;
import com.example.justshow.Activity.Home.Profile.ProfileViewModel;
import com.example.justshow.Items.ProfileItem;
import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.time.LocalDate;
import java.util.List;

import info.movito.themoviedbapi.model.tv.TvSeries;

public class SeriesUpcomingListAdapter extends RecyclerView.Adapter<SeriesUpcomingViewHolder>{

    private final List<TvSeries> seriesList;
    private final Activity activity;
    ShineButton favorite;

    public SeriesUpcomingListAdapter(final List<TvSeries> seriesList, final Activity activity){
        this.seriesList = seriesList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public SeriesUpcomingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.seriestv_upcoming_row,parent,false);
        favorite = layoutView.findViewById(R.id.favoriteUpcomingSeriesTvFilterButton);
        return new SeriesUpcomingViewHolder(layoutView);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull SeriesUpcomingViewHolder holder, int position) {
        TvSeries currentUpcomingSeriesItem = seriesList.get(position);

        if (currentUpcomingSeriesItem.getPosterPath() != null && !currentUpcomingSeriesItem.getPosterPath().equals("")){
            holder.loadImage("https://image.tmdb.org/t/p/original" + currentUpcomingSeriesItem.getPosterPath(), activity);
        }

        holder.titleUpcomingTvRowTextView.setText(currentUpcomingSeriesItem.getName());

        if (currentUpcomingSeriesItem.getOverview().isEmpty()){
            holder.plotUpcomingTvRowTextView.setText("Nessuna trama disponibile");
        }else{
            holder.plotUpcomingTvRowTextView.setText(currentUpcomingSeriesItem.getOverview());
        }

        holder.dateUpcomingTvRowTextView.setText(LocalDate.now().toString());

        holder.itemView.setOnClickListener(v -> {
            IntentDetailsCreator intentCreator = new IntentDetailsCreator();
            Intent intent = intentCreator.createIntentSeries(activity, "Upcoming", currentUpcomingSeriesItem.getId());
            activity.startActivity(intent);
        });

        ProfileViewModel profileViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(ProfileViewModel.class);
        ProfileItem activeProfile = ((JustShow) activity.getApplication()).getActiveProfile();

        favorite.setChecked(activeProfile != null && activeProfile.getFavoriteTvSeriesList().contains(currentUpcomingSeriesItem));

        favorite.setOnClickListener(v -> {
            if (activeProfile != null) {
                List<TvSeries> newList = activeProfile.getFavoriteTvSeriesList();
                if (newList.contains(currentUpcomingSeriesItem)){
                    newList.remove(currentUpcomingSeriesItem);
                }else {
                    newList.add(currentUpcomingSeriesItem);
                }
                activeProfile.setFavoriteTvSeriesList(newList);
                profileViewModel.updateFavoriteList(activeProfile.getFavoriteMovieList(),activeProfile.getFavoriteTvSeriesList(),activeProfile.getId());
            }else{
                favorite.setChecked(false);
                Toast.makeText(activity,"Necessario effettuare il login", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return seriesList.size();
    }
}
