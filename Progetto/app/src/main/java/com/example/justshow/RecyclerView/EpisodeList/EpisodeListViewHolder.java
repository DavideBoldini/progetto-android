package com.example.justshow.RecyclerView.EpisodeList;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.justshow.R;

public class EpisodeListViewHolder extends RecyclerView.ViewHolder{

    ImageView imageEpisodeView;
    TextView titleTextView;
    TextView dateTextView;
    TextView plotTextView;
    TextView ratingTextView;


    public EpisodeListViewHolder(@NonNull View itemView) {
        super(itemView);

        imageEpisodeView = itemView.findViewById(R.id.thumbnailEpisode);
        dateTextView = itemView.findViewById(R.id.dateEpisode);
        plotTextView = itemView.findViewById(R.id.plotEpisode);
        titleTextView = itemView.findViewById(R.id.titleEpisode);
        ratingTextView = itemView.findViewById(R.id.episodeRating);

    }
}
