package com.example.justshow.RecyclerView.SeriesList;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.Activity.Details.IntentDetailsCreator;
import com.example.justshow.Activity.Home.Profile.ProfileViewModel;
import com.example.justshow.Items.ProfileItem;
import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.List;

import info.movito.themoviedbapi.model.tv.TvSeries;

public class SeriesTopListAdapter extends RecyclerView.Adapter<SeriesTopViewHolder>{

    private final List<TvSeries> seriesList;
    private final Activity activity;
    ShineButton favorite;

    public SeriesTopListAdapter(final List<TvSeries> seriesList, final Activity activity){
        this.seriesList = seriesList;
        this.activity = activity;
    }


    @NonNull
    @Override
    public SeriesTopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.seriestv_top_row,parent,false);

        favorite = layoutView.findViewById(R.id.favoriteTopSeriesTvFilterButton);

        return new SeriesTopViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull SeriesTopViewHolder holder, int position) {
        TvSeries currentTopSeriesItem = seriesList.get(position);

        if (currentTopSeriesItem.getPosterPath() != null && !currentTopSeriesItem.getPosterPath().equals("")){
            holder.loadImage("https://image.tmdb.org/t/p/original" + currentTopSeriesItem.getPosterPath(), activity);
        }

        holder.titleTopTvRowTextView.setText(currentTopSeriesItem.getName());

        if (currentTopSeriesItem.getOverview().isEmpty()){
            holder.plotTopTvRowTextView.setText("Nessuna trama disponibile");
        }else{
            holder.plotTopTvRowTextView.setText(currentTopSeriesItem.getOverview());
        }

        holder.ratingTopTvRowTextView.append(" " + currentTopSeriesItem.getVoteAverage());

        holder.itemView.setOnClickListener(v -> {
            IntentDetailsCreator intentCreator = new IntentDetailsCreator();
            Intent intent = intentCreator.createIntentSeries(activity, "Top", currentTopSeriesItem.getId());
            activity.startActivity(intent);
        });

        ProfileViewModel profileViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(ProfileViewModel.class);
        ProfileItem activeProfile = ((JustShow) activity.getApplication()).getActiveProfile();

        favorite.setChecked(activeProfile != null && activeProfile.getFavoriteTvSeriesList().contains(currentTopSeriesItem));

        favorite.setOnClickListener(v -> {
            if (activeProfile != null) {
                List<TvSeries> newList = activeProfile.getFavoriteTvSeriesList();
                if (newList.contains(currentTopSeriesItem)){
                    newList.remove(currentTopSeriesItem);
                }else {
                    newList.add(currentTopSeriesItem);
                }
                activeProfile.setFavoriteTvSeriesList(newList);
                profileViewModel.updateFavoriteList(activeProfile.getFavoriteMovieList(),activeProfile.getFavoriteTvSeriesList(),activeProfile.getId());
            }else{
                favorite.setChecked(false);
                Toast.makeText(activity,"Necessario effettuare il login", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return seriesList.size();
    }
}
