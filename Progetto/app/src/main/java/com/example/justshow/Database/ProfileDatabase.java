package com.example.justshow.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.justshow.Items.ProfileItem;
import com.example.justshow.Utilities.Converters;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {ProfileItem.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class ProfileDatabase extends RoomDatabase {

    public abstract ProfileDAO profileDAO();
    private static volatile ProfileDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;

    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static ProfileDatabase getDatabase(final Context context) {
        if (INSTANCE == null){
            synchronized (ProfileDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),ProfileDatabase.class,"profile_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
