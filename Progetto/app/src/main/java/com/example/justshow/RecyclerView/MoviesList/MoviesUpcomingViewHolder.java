package com.example.justshow.RecyclerView.MoviesList;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.justshow.R;
import com.sackcentury.shinebuttonlib.ShineButton;

public class MoviesUpcomingViewHolder extends RecyclerView.ViewHolder{

    ImageView thumbnailUpcomingMovieRowImageView;
    ShineButton favoriteUpcomingMovieFilterButton;
    TextView titleUpcomingMovieRowTextView;
    TextView plotUpcomingMovieRowTextView;
    View dividerUpcomingMovieRowView;
    TextView dateUpcomingMovieRowTextView;

    public MoviesUpcomingViewHolder(@NonNull View itemView) {
        super(itemView);

        thumbnailUpcomingMovieRowImageView = itemView.findViewById(R.id.thumbnailUpcomingMoviesRow);
        favoriteUpcomingMovieFilterButton = itemView.findViewById(R.id.favoriteUpcomingMoviesFilterButton);
        titleUpcomingMovieRowTextView = itemView.findViewById(R.id.titleUpcomingMoviesRow);
        plotUpcomingMovieRowTextView = itemView.findViewById(R.id.plotUpcomingMoviesRow);
        dividerUpcomingMovieRowView = itemView.findViewById(R.id.plotUpcomingMoviesRowDivider);
        dateUpcomingMovieRowTextView = itemView.findViewById(R.id.dateUpcomingMoviesRow);

    }


    public void loadImage(String posterPath, Activity activity){
        Glide.with(activity)
                .load(posterPath)
                .error(R.drawable.ic_baseline_image_not_supported_24)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(thumbnailUpcomingMovieRowImageView);
    }


}
