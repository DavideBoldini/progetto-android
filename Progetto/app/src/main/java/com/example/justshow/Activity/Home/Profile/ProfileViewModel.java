package com.example.justshow.Activity.Home.Profile;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.justshow.Database.ProfileRepository;

import java.util.HashMap;
import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.tv.TvSeries;

public class ProfileViewModel extends AndroidViewModel {

    private final ProfileRepository repository;

    public ProfileViewModel(@NonNull Application application) {
        super(application);
        repository = new ProfileRepository(application);
    }

    public void updateFavoriteList(List<MovieDb> filmList, List<TvSeries> tvList, int id){
        repository.updateFavoriteList(filmList, tvList, id);
    }

    public void updateWatchlist(HashMap<String,String> watchlist, int id){
        repository.updateWatchlist(watchlist,id);
    }

    public void updateProfileDetails(String thumbnail,String name, String surname, String birthDate, String address, String city, String country, String email, String password){
        repository.updateProfileDetails(thumbnail, name, surname, birthDate, address, city, country, email, password);
    }
}
