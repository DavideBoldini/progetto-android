package com.example.justshow.Activity.Register;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.chivorn.smartmaterialspinner.SmartMaterialSpinner;
import com.example.justshow.Activity.Home.MainActivity;
import com.example.justshow.Activity.Home.Profile.ProfileViewModel;
import com.example.justshow.Activity.Login.LoginViewModel;
import com.example.justshow.Items.ProfileItem;
import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.example.justshow.Utilities.Utilities;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


public class RegisterFragment extends Fragment {

    private SmartMaterialSpinner<String> countrySpinner;
    private List<String> countryList;
    private String countrySelected = null;
    private TextInputEditText nameTextInputEditText;
    private TextInputEditText surnameTextInputEditText;
    private ShapeableImageView profileImage;
    private SingleDateAndTimePicker birthdatePicker;
    private TextInputEditText addressTextInputEditText;
    private TextInputEditText cityTextInputEditText;
    private TextInputEditText emailTextInputEditText;
    private TextInputEditText passwordTextInputEditText;
    private ProfileItem activeProfile;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_page,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initSpinner(view);

        nameTextInputEditText = view.findViewById(R.id.nameInputEdit);
        surnameTextInputEditText = view.findViewById(R.id.surnameInputEdit);

        birthdatePicker = view.findViewById(R.id.birthdayInput);

        Button addProfilePhoto = view.findViewById(R.id.changeProfileImageRegisterButton);
        addProfilePhoto.setOnClickListener(v -> ImagePicker.with(getActivity())
                .crop()
                .compress(1024)
                .maxResultSize(1080,1080)
                .start());

        profileImage = view.findViewById(R.id.registerProfileImage);
        profileImage.setImageResource(R.drawable.ic_baseline_person_24);

        RegisterViewModel registerViewModel = new ViewModelProvider((ViewModelStoreOwner) getActivity()).get(RegisterViewModel.class);
        registerViewModel.getBitmap().observe(getViewLifecycleOwner(), new Observer<Bitmap>() {
            @Override
            public void onChanged(Bitmap bitmap) {
                profileImage.setImageBitmap(bitmap);
            }
        });

        LoginViewModel loginViewModel = new ViewModelProvider((ViewModelStoreOwner) getActivity()).get(LoginViewModel.class);
        ProfileViewModel profileViewModel = new ViewModelProvider((ViewModelStoreOwner) getActivity()).get(ProfileViewModel.class);

        addressTextInputEditText = view.findViewById(R.id.addressInputEdit);
        cityTextInputEditText = view.findViewById(R.id.cityInputEdit);

        Button showGMaps = view.findViewById(R.id.showGMaps);
        showGMaps.setOnClickListener(v -> new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String str_location = Objects.requireNonNull(addressTextInputEditText.getText()).toString();
                String city = Objects.requireNonNull(cityTextInputEditText.getText()).toString();
                String country = countrySelected;
                String map = "http://maps.google.co.in/maps?q=" + str_location + "," + city + "," + country;
                if (!str_location.equals("") && !city.equals("") && country != null){
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }else {
                    Toast.makeText(getContext(),"Località non valida", Toast.LENGTH_SHORT).show();
                }

            }
        },300));

        emailTextInputEditText = view.findViewById(R.id.emailInputEdit);
        passwordTextInputEditText = view.findViewById(R.id.passwordInputEdit);

        Button cancelRegisterButton = view.findViewById(R.id.cancelRegisterButton);
        cancelRegisterButton.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), MainActivity.class);
            getContext().startActivity(intent);
        });

        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null && extras.containsKey("Type")){
            String type = extras.getString("Type");
            if (type.equals("Edit Profile")){
                activeProfile = ((JustShow) getActivity().getApplication()).getActiveProfile();
                Bitmap profileImageBitmap = Utilities.getImageBitmap(getActivity(),Uri.parse(activeProfile.getFotoProfilo()));
                profileImage.setImageBitmap(profileImageBitmap);
                registerViewModel.setImageBitmap(profileImageBitmap);

                TextInputLayout nameTextInputLayout = view.findViewById(R.id.nameInput);
                nameTextInputLayout.getEditText().setText(activeProfile.getNome());

                TextInputLayout surnameTextInputLayout = view.findViewById(R.id.surnameInput);
                surnameTextInputLayout.getEditText().setText(activeProfile.getCognome());

                String dateString = activeProfile.getDataNascita();
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALY);
                Date date = null;
                try {
                    date = format.parse(dateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                birthdatePicker.setDefaultDate(date);


                TextInputLayout addressTextInputLayout = view.findViewById(R.id.addressInput);
                addressTextInputLayout.getEditText().setText(activeProfile.getIndirizzo());

                TextInputLayout cityTextInputLayout = view.findViewById(R.id.cityInput);
                cityTextInputLayout.getEditText().setText(activeProfile.getCittà());

                int position = countrySpinner.getItem().indexOf(activeProfile.getNazione());
                countrySpinner.setSelection(position);

                TextInputLayout emailTextInputLayout = view.findViewById(R.id.emailInput);
                emailTextInputLayout.getEditText().setText(activeProfile.getEmail());

                TextInputLayout passwordTextInputLayout = view.findViewById(R.id.passwordInput);
                passwordTextInputLayout.getEditText().setText(activeProfile.getPassword());

            }
        }


        Button saveRegisterButton = view.findViewById(R.id.saveRegisterButton);
        saveRegisterButton.setOnClickListener(v -> {

            Bundle extras1 = getActivity().getIntent().getExtras();

            String name = Objects.requireNonNull(nameTextInputEditText.getText()).toString();
            String surname = Objects.requireNonNull(surnameTextInputEditText.getText()).toString();
            String email = Objects.requireNonNull(emailTextInputEditText.getText()).toString();
            String pass = Objects.requireNonNull(passwordTextInputEditText.getText()).toString();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sdf.format(birthdatePicker.getDate());
            String address = Objects.requireNonNull(addressTextInputEditText.getText()).toString();
            String city = Objects.requireNonNull(cityTextInputEditText.getText()).toString();
            if (!email.equals("") && !pass.equals("") && !name.equals("") && !surname.equals("")
                    && !address.equals("") && !city.equals("") && countrySelected != null ){
                if (city.matches(".*\\d.*")){
                    Toast.makeText(getContext(),"Errore campo Città", Toast.LENGTH_SHORT).show();
                }else {
                    try {
                        Bitmap bitmap = registerViewModel.getBitmap().getValue();
                        String imageUriString;
                        if (bitmap != null){
                            imageUriString = String.valueOf(saveImage(bitmap, requireActivity()));
                        }else {
                            imageUriString = "ic_launcher_foreground";
                        }

                        if (extras1 != null && extras1.containsKey("Type")){
                            String type = extras1.getString("Type");
                            if (type != null && type.equals("Edit Profile")){
                                    loginViewModel.getProfileByEmail(email).observe(getViewLifecycleOwner(), new Observer<List<ProfileItem>>() {
                                        @Override
                                        public void onChanged(List<ProfileItem> profileItems) {
                                            if (getViewLifecycleOwner().getLifecycle().getCurrentState()== Lifecycle.State.RESUMED){

                                                    profileViewModel.updateProfileDetails(imageUriString,name,surname,date,address,city,countrySelected,email,pass);

                                                    activeProfile = ((JustShow) getActivity().getApplication()).getActiveProfile();
                                                    activeProfile.setFotoProfilo(imageUriString);
                                                    activeProfile.setNome(name);
                                                    activeProfile.setCognome(surname);
                                                    activeProfile.setDataNascita(date);
                                                    activeProfile.setIndirizzo(address);
                                                    activeProfile.setCittà(city);
                                                    activeProfile.setNazione(countrySelected);
                                                    activeProfile.setEmail(email);
                                                    activeProfile.setPassword(pass);

                                                    Toast.makeText(getContext(),"Modifiche avvenute con successo", Toast.LENGTH_SHORT).show();
                                                    Intent intent = new Intent(getContext(), MainActivity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(intent);
                                                }
                                            }

                                    });
                                }else {
                                loginViewModel.getProfileByEmail(email).observe(getViewLifecycleOwner(), new Observer<List<ProfileItem>>() {
                                    @Override
                                    public void onChanged(List<ProfileItem> profileItems) {
                                        if (getViewLifecycleOwner().getLifecycle().getCurrentState()== Lifecycle.State.RESUMED){
                                            if (profileItems != null && profileItems.size() != 0){
                                                Toast.makeText(getContext(),"Utente già registrato", Toast.LENGTH_SHORT).show();
                                            }else{
                                                registerViewModel.addProfileItem(new ProfileItem(imageUriString,
                                                        name,
                                                        surname,
                                                        date,
                                                        address,
                                                        city,
                                                        countrySelected,
                                                        email,
                                                        pass,
                                                        new ArrayList<>(),
                                                        new ArrayList<>(),
                                                        new HashMap<>()
                                                ));
                                                registerViewModel.setImageBitmap(null);
                                                Toast.makeText(getContext(),"Registrazione avvenuta con successo", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getContext(), MainActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }else {
                Toast.makeText(getContext(),"Necessario inserire tutti i campi", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private Uri saveImage(Bitmap bitmap, Activity activity) throws IOException{
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY).format(new Date());
        String name = "JPEG_" + timeStamp + "_.png";

        ContentResolver resolver = activity.getContentResolver();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name + ".jpg");
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE,"image/jpg");
        Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
        Log.d("RegisterFragment", String.valueOf(imageUri));
        OutputStream fos = resolver.openOutputStream(imageUri);

        bitmap.compress(Bitmap.CompressFormat.JPEG,100,fos);
        if (fos != null){
            fos.close();
        }
        return imageUri;
    }

    private void initSpinner(View view){
        countrySpinner = view.findViewById(R.id.countrySpinner);
        countryList = new ArrayList<>();

        countryList.add("Italia");
        countryList.add("USA");
        countryList.add("Francia");
        countryList.add("Germania");
        countryList.add("UK");

        countrySpinner.setItem(countryList);

        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countrySelected = countryList.get(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
}
