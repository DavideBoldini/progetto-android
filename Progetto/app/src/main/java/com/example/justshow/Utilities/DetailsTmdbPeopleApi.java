package com.example.justshow.Utilities;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.model.people.PersonCredits;
import info.movito.themoviedbapi.tools.ApiUrl;

public class DetailsTmdbPeopleApi extends AbstractPersonalTmdb {

    public static final String TMDB_METHOD_PERSON = "person";


    DetailsTmdbPeopleApi(TmdbApi tmdbApi) {
        super(tmdbApi);
    }

    public PersonCredits getCombinedPersonCredits(int personId,String language) {
        ApiUrl apiUrl = new ApiUrl(TMDB_METHOD_PERSON, personId, "combined_credits");
        apiUrl.addLanguage(language);

        return mapJsonResult(apiUrl, PersonCredits.class);
    }
}
