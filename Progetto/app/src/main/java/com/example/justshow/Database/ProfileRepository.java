package com.example.justshow.Database;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.justshow.Items.ProfileItem;

import java.util.HashMap;
import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.tv.TvSeries;

public class ProfileRepository {

    private final ProfileDAO profileDAO;
    private LiveData<List<ProfileItem>> profileList;

    public ProfileRepository(Application application){
        ProfileDatabase db = ProfileDatabase.getDatabase(application);
        profileDAO = db.profileDAO();
        profileList = profileDAO.getAllProfile();
    }

    public LiveData<List<ProfileItem>> getAllProfileList(){
        profileList = profileDAO.getAllProfile();
        return profileList;
    }

    public LiveData<List<ProfileItem>> getProfileByEmail(String email){
        profileList = profileDAO.getProfile(email);
        return profileList;
    }

    public LiveData<List<ProfileItem>> checkLogin(String email, String password){
        profileList = profileDAO.checkLogin(email,password);
        return profileList;
    }

    public void addProfileItem(final ProfileItem profile){
        ProfileDatabase.databaseWriteExecutor.execute(() -> profileDAO.addProfileItem(profile));
    }

    public void updateFavoriteList(List<MovieDb> filmList, List<TvSeries> tvList, int id){
        ProfileDatabase.databaseWriteExecutor.execute(() -> profileDAO.updateFavoriteList(filmList, tvList, id));
    }

    public void updateWatchlist(HashMap<String,String> watchlist, int id){
        ProfileDatabase.databaseWriteExecutor.execute(() -> profileDAO.updateWatchlist(watchlist, id));
    }

    public void updateProfileDetails(String thumbnail,String name, String surname, String birthDate, String address, String city, String country, String email, String password){
        ProfileDatabase.databaseWriteExecutor.execute(() -> profileDAO.updateProfileDetails(thumbnail, name, surname, birthDate, address, city, country, email, password));
    }

}
