package com.example.justshow.Activity.Details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.justshow.Utilities.MoviesApi;
import com.example.justshow.Utilities.TvSeriesApi;

import java.util.concurrent.ExecutionException;

import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.tv.TvSeries;

public class IntentDetailsCreator {


    public Intent createIntentSeries(Context context, String type, int id){
        Intent intent = new Intent(context, DetailsActivity.class);

        TvSeries serie = null;

        try {
            serie = (TvSeries) new TvSeriesApi().execute("Get dettagli Serie", String.valueOf(id)).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }


        Bundle extras = new Bundle();
        extras.putSerializable("Tv", serie);
        extras.putString("Type", type);
        extras.putInt("NumSeason", serie.getNumberOfSeasons());
        intent.putExtras(extras);

        return intent;
    }

    public Intent createIntentMovies(Context context, String type, int id){
        Intent intent = new Intent(context, DetailsActivity.class);

        MovieDb movie = null;

        try {
            movie = (MovieDb) new MoviesApi().execute("Get dettagli Film", String.valueOf(id)).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        Bundle extras = new Bundle();
        extras.putSerializable("Movie", movie);
        extras.putString("Type", type);
        intent.putExtras(extras);

        return intent;
    }
}
