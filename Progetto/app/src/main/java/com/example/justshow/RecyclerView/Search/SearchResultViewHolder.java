package com.example.justshow.RecyclerView.Search;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.justshow.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SearchResultViewHolder extends RecyclerView.ViewHolder{

    ImageView thumbnailResultRowImageView;
    TextView titleResultRowTextView;
    TextView plotResultRowTextView;
    View dividerResultRowView;
    TextView ratingResultRowTextView;

    public SearchResultViewHolder(@NonNull View itemView) {
        super(itemView);

        thumbnailResultRowImageView = itemView.findViewById(R.id.thumbnailResult);
        titleResultRowTextView = itemView.findViewById(R.id.titleResult);
        plotResultRowTextView = itemView.findViewById(R.id.plotResult);
        dividerResultRowView = itemView.findViewById(R.id.resultDivider);
        ratingResultRowTextView = itemView.findViewById(R.id.ratingResult);

    }
}
