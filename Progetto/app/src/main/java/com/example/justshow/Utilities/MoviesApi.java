package com.example.justshow.Utilities;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.TmdbDiscover;
import info.movito.themoviedbapi.TmdbMovies;
import info.movito.themoviedbapi.TmdbReviews;
import info.movito.themoviedbapi.TmdbSearch;
import info.movito.themoviedbapi.model.Discover;
import info.movito.themoviedbapi.model.Genre;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.MovieImages;
import info.movito.themoviedbapi.model.Reviews;
import info.movito.themoviedbapi.model.core.MovieResultsPage;

import org.apache.commons.lang3.StringUtils;

public class MoviesApi extends AsyncTask<String, Void, Object> {

    TmdbApi tmdb;
    public ApiResponse delegate;

    public MoviesApi(){}
    public MoviesApi(ApiResponse delegate){
        this.delegate = delegate;
    }

    @Override
    protected Object doInBackground(String... strings) {
        tmdb = new TmdbApi("db981d35add49fc538c2afdd56fd00cf");
        switch (strings[0]){
            case "PopMovies":
                return this.getPopularMovies(tmdb);
            case "TopMovies":
                return this.getTopRatedMovies(tmdb);
            case "UpcomingMovies":
                return this.getUpcomingMovies(tmdb);
            case "Cerca Film":
                return this.searchFilmList(tmdb,strings[1]);
            case "Get dettagli Film":
                return this.getDettagliMovie(tmdb, strings[1]);
            case "Get dettagli Film by Title":
                return this.getDettagliMovieByTitle(tmdb, strings[1]);
            case "Get Immagini Film":
                return this.getMovieImages(tmdb,strings[1]);
            case "Get Review Film":
                return this.getReviewMovie(tmdb,strings[1]);
            case "Cerca Film dettagliata":
                return this.getMovieDeepSearch(tmdb,strings[1],strings[2],strings[3],strings[4],strings[5],strings[6],strings[7],strings[8],strings[9],strings[10],strings[11],strings[12],strings[13], strings[14],
                        strings[15], strings[16], strings[17]);
        }
        return Collections.emptyList();
    }

    @Override
    protected void onPostExecute(Object o) {
        if (delegate != null){
            delegate.processFinish(o);
        }
    }

    private List<MovieDb> getMovieDeepSearch(TmdbApi tmdb, String titleIn, String genre1, String genre2, String genre3, String genre4,
                                             String genre5, String genre6, String genre7, String genre8, String genre9, String genre10, String genre11, String minVote, String maxVote, String daYear, String aYear, String page) {

        List<MovieDb> resList = new ArrayList<>();

        List<String> genreList = new ArrayList<>();
        genreList.add(genre1);
        genreList.add(genre2);
        genreList.add(genre3);
        genreList.add(genre4);
        genreList.add(genre5);
        genreList.add(genre6);
        genreList.add(genre7);
        genreList.add(genre8);
        genreList.add(genre9);
        genreList.add(genre10);
        genreList.add(genre11);

        genreList.removeAll(Collections.singleton(null));

        if (titleIn != null){
            ArrayList<MovieDb> resListSearch = new ArrayList<>(this.searchFilmListByPage(tmdb, titleIn,page));
            for(MovieDb res : resListSearch){
                MovieDb film = this.getDettagliMovie(tmdb,String.valueOf(res.getId()));
                if (film.getVoteAverage() >= Float.parseFloat(minVote) && film.getVoteAverage() <= Float.parseFloat(maxVote)){
                    if (film.getReleaseDate() != null && !film.getReleaseDate().equals("")){
                        String[] splits = film.getReleaseDate().split("-");
                        int year = Integer.parseInt(splits[0]);
                        if(year >= Integer.parseInt(daYear) && year <= Integer.parseInt(aYear)){
                            if (!genreList.isEmpty()){
                                for (Genre genre : film.getGenres()) {
                                    if (genreList.contains(String.valueOf(genre.getId()))){
                                        resList.add(film);
                                        break;
                                    }
                                }
                            }else{
                                resList.add(film);
                            }
                        }
                    }
                }
            }
        }else{
            TmdbDiscover discoverTmdb = tmdb.getDiscover();
            Discover discover = new Discover();
            discover.voteAverageGte(Float.parseFloat(minVote));
            discover.releaseDateGte(daYear +"-01-01");
            discover.releaseDateLte(aYear+"-12-31");
            discover.includeAdult(false);
            discover.language("it-IT");
            if(genreList.size() > 1){
                discover.withGenres(StringUtils.join(genreList,"|"));
            }else if (genreList.size() == 1){
                discover.withGenres(genreList.get(0));
            }

            MovieResultsPage resPage = discoverTmdb.getDiscover(discover);
            int totPage = resPage.getTotalPages();
            for (int i = 2; i<= totPage+1; i++){
                if (resList.size() >= 500){
                    break;
                }
                discover.page(i-1);
                resPage = discoverTmdb.getDiscover(discover);
                resList.addAll(resPage.getResults());
            }

            Iterator<MovieDb> iter = resList.iterator();
            while(iter.hasNext()){
                MovieDb movie = iter.next();
                if (movie.getVoteAverage() > Float.parseFloat(maxVote)){
                    iter.remove();
                }

                if (movie.getReleaseDate() != null && !movie.getReleaseDate().equals("")){
                    String[] splits = movie.getReleaseDate().split("-");
                    int year = Integer.parseInt(splits[0]);
                    if (year > Integer.parseInt(aYear) || year < Integer.parseInt(daYear)){
                        iter.remove();
                    }
                }

            }
        }
        return resList;
    }

    private List<MovieDb> getTopRatedMovies(final TmdbApi tmdb){
        MovieResultsPage resPage;
        TmdbMovies movies = tmdb.getMovies();
        resPage = movies.getTopRatedMovies("it-IT",0);
        return resPage.getResults();
    }

    // Popular Requests
    private List<MovieDb> getPopularMovies(final TmdbApi tmdb){
        TmdbMovies movies = tmdb.getMovies();
        MovieResultsPage resPage = movies.getPopularMovies("it-IT",0);
        return resPage.getResults();
    }

    // Upcoming Requests
    private List<MovieDb> getUpcomingMovies(final TmdbApi tmdb){
        TmdbMovies movies = tmdb.getMovies();
        MovieResultsPage resPage = movies.getUpcoming("it-IT",0, null);
        return resPage.getResults();
    }

    private List<MovieDb> searchFilmList(final TmdbApi tmdb, String filmName){
        TmdbSearch search = tmdb.getSearch();
        MovieResultsPage resPage = search.searchMovie(filmName,0,"it-IT",false,0);
        return resPage.getResults();
    }

    private List<MovieDb> searchFilmListByPage(final TmdbApi tmdb, String filmName, String page){
        TmdbSearch search = tmdb.getSearch();
        MovieResultsPage resPage = search.searchMovie(filmName,0,"it-IT",false,Integer.parseInt(page));
        return resPage.getResults();
    }

    private MovieDb getDettagliMovie(TmdbApi tmdb, String idIn){
        TmdbMovies movies = tmdb.getMovies();
        int id = Integer.parseInt(idIn);
        MovieDb res = movies.getMovie(id, "it-IT", TmdbMovies.MovieMethod.videos, TmdbMovies.MovieMethod.recommendations,
                TmdbMovies.MovieMethod.credits, TmdbMovies.MovieMethod.reviews, TmdbMovies.MovieMethod.similar);
        return res;
    }

    private MovieDb getDettagliMovieByTitle(TmdbApi tmdb, String title) {
        MovieDb res = null;
        TmdbSearch search = tmdb.getSearch();
        TmdbMovies movies = tmdb.getMovies();
        MovieResultsPage resPage = search.searchMovie(title,0,"it-IT",false,0);
        for(MovieDb film : resPage.getResults()){
            if (film.getTitle().equals(title)){
                res = movies.getMovie(film.getId(),"it-IT",TmdbMovies.MovieMethod.videos, TmdbMovies.MovieMethod.recommendations,
                        TmdbMovies.MovieMethod.credits, TmdbMovies.MovieMethod.reviews, TmdbMovies.MovieMethod.similar);
                return res;
            }
        }
        return res;
    }

    private MovieImages getMovieImages(TmdbApi tmdb, String filmName){
        TmdbSearch search = tmdb.getSearch();
        MovieResultsPage resPage = search.searchMovie(filmName,0,"it-IT",false,0);

        TmdbMovies movies = tmdb.getMovies();
        MovieImages res = null;
        for(MovieDb elem : resPage.getResults()){
            if(elem.getTitle().equals(filmName)){
                res = movies.getImages(elem.getId(),null);
                return res;
            }
        }
        return res;
    }


    private List<Reviews> getReviewMovie(TmdbApi tmdb, String filmName){
        TmdbSearch search = tmdb.getSearch();
        MovieResultsPage resPage = search.searchMovie(filmName,0,"it-IT",false,0);

        TmdbMovies movies = tmdb.getMovies();
        MovieDb res = null;
        for(MovieDb elem : resPage.getResults()){
            if(elem.getTitle().equals(filmName)){
                res = movies.getMovie(elem.getId(),"it-IT");
                break;
            }
        }
        TmdbReviews reviews = tmdb.getReviews();
        return reviews.getReviews(res.getId(),null,0).getResults();
    }

}
