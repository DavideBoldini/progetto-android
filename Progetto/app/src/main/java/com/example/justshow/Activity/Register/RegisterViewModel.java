package com.example.justshow.Activity.Register;

import android.app.Application;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.justshow.Database.ProfileRepository;
import com.example.justshow.Items.ProfileItem;

public class RegisterViewModel extends AndroidViewModel {

    private final MutableLiveData<Bitmap> imageBitmap = new MutableLiveData<>();
    private final ProfileRepository repository;

    public RegisterViewModel(@NonNull Application application) {
        super(application);
        repository = new ProfileRepository(application);
    }

    public void setImageBitmap(Bitmap bitmap){
        imageBitmap.setValue(bitmap);
    }

    public LiveData<Bitmap> getBitmap() {
        return imageBitmap;
    }

    public void addProfileItem(ProfileItem item){
        repository.addProfileItem(item);
    }

}
