package com.example.justshow.Activity.Details.EpisodeList;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager2.widget.ViewPager2;

import com.example.justshow.Activity.Home.MainActivity;
import com.example.justshow.Activity.Home.Profile.ProfileFragment;
import com.example.justshow.Activity.Home.Profile.WelcomeActivity;
import com.example.justshow.Activity.Search.SearchFragment;
import com.example.justshow.Items.ProfileItem;
import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;


public class EpisodeListActivity extends AppCompatActivity implements NavigationBarView.OnItemSelectedListener {

    BottomNavigationView bottomNavigationView;
    boolean userLogged;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.episode_list_main);

        bottomNavigationView = findViewById(R.id.bottomAppBar);
        bottomNavigationView.setOnItemSelectedListener(this);
        uncheckAllItems();

        userLogged = ((JustShow) this.getApplication()).getUserLogged();
        if (userLogged){
            ProfileItem activeProfile = ((JustShow) getApplication()).getActiveProfile();
            Menu menu = bottomNavigationView.getMenu();
            menu.findItem(R.id.profile_bottom).setTitle(activeProfile.getNome());
        }

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        String title = extras.getString("Title");
        int numSeason = extras.getInt("NumSeason");

        TabLayout tabLayout = findViewById(R.id.tabLayoutEpList);
        ViewPager2 viewPager2 = findViewById(R.id.viewPagerEp);

        ViewPagerEpisodesAdapter adapter = new ViewPagerEpisodesAdapter(this, numSeason,title);
        viewPager2.setAdapter(adapter);

        new TabLayoutMediator(tabLayout, viewPager2, (tab, position) -> {
            tab.setText(String.valueOf(position+1));
        }).attach();
    }

    @Override
    protected void onResume() {
        super.onResume();
        uncheckAllItems();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();

        int id = item.getItemId();
        Intent intent;
        Fragment fragment = null;
        String tag = "";
        switch (id){

            case R.id.home_bottom:
                intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(intent);
                return true;
            case R.id.search_bottom:
                fragment = new SearchFragment();
                tag = "SearchFragment";
                break;
            case R.id.profile_bottom:
                if (!userLogged){
                    intent = new Intent(this, WelcomeActivity.class);
                    this.startActivity(intent);
                    return true;
                }else {
                    fragment = new ProfileFragment();
                    tag = "Profile Fragment";
                    break;
                }
        }
        fragmentTransaction.replace(R.id.principalEpView, fragment, tag);
        fragmentTransaction.commit();
        return true;
    }

    private void uncheckAllItems(){
        bottomNavigationView.getMenu().setGroupCheckable(0,true,false);
        Menu menu = bottomNavigationView.getMenu();
        for (int i = 0; i< menu.size(); i++){
            menu.getItem(i).setChecked(false);
        }
        menu.setGroupCheckable(0,true,true);
    }
}
