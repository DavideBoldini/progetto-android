package com.example.justshow.Activity.Home.TopMovies;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.R;
import com.example.justshow.RecyclerView.MoviesList.MoviesTopListAdapter;
import com.example.justshow.Utilities.MoviesUtilities;

import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;

public class TopMoviesFragment extends Fragment {

    private RecyclerView recyclerView;
    private MoviesTopListAdapter moviesTopListAdapter;
    LinearLayoutManager llm = new LinearLayoutManager(getActivity());

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.top_movies,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Activity activity = getActivity();
        if (activity != null){
            setRecyclerView(activity);
        }else{
            Log.e("TopMovies.java","Activity is null");
        }
    }

    private void setRecyclerView(final Activity activity){
        recyclerView = getView().findViewById(R.id.allTopMoviesListRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(llm);
        recyclerView.setNestedScrollingEnabled(false);
        List<MovieDb> list = MoviesUtilities.getTopMovies();
        moviesTopListAdapter = new MoviesTopListAdapter(list, activity);
        recyclerView.setAdapter(moviesTopListAdapter);
    }
}
