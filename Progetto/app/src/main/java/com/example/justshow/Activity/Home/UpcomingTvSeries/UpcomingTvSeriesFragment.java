package com.example.justshow.Activity.Home.UpcomingTvSeries;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.R;
import com.example.justshow.RecyclerView.SeriesList.SeriesUpcomingListAdapter;
import com.example.justshow.Utilities.TvSeriesUtilities;

import java.util.List;

import info.movito.themoviedbapi.model.tv.TvSeries;

public class UpcomingTvSeriesFragment extends Fragment {

    private RecyclerView recyclerView;
    private SeriesUpcomingListAdapter seriesUpcomingListAdapter;
    LinearLayoutManager llm = new LinearLayoutManager(getActivity());

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.upcoming_tvseries, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();
        if (activity != null){
            setRecyclerView(activity);
        }else {
            Log.e("UpcomingTvSeries.java", "Activity is null");
        }
    }

    private void setRecyclerView(final Activity activity) {
        recyclerView = getView().findViewById(R.id.allUpcomingTvSeriesListRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(llm);
        List<TvSeries> list = TvSeriesUtilities.getUpcomingTv();
        seriesUpcomingListAdapter = new SeriesUpcomingListAdapter(list,activity);
        recyclerView.setAdapter(seriesUpcomingListAdapter);
    }

}
