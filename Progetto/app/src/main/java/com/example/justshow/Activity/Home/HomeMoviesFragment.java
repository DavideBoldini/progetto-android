package com.example.justshow.Activity.Home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.justshow.Activity.Details.DetailsActivity;
import com.example.justshow.Activity.Home.News.NewsFragment;
import com.example.justshow.Activity.Home.PopularMovies.PopularMoviesFragment;
import com.example.justshow.Activity.Home.TopMovies.TopMoviesFragment;
import com.example.justshow.Activity.Home.UpcomingMovies.UpcomingMoviesFragment;
import com.example.justshow.R;
import com.example.justshow.Utilities.ApiResponse;
import com.example.justshow.Utilities.MoviesApi;
import com.example.justshow.Utilities.MoviesUtilities;
import com.kwabenaberko.newsapilib.NewsApiClient;
import com.kwabenaberko.newsapilib.models.Article;
import com.kwabenaberko.newsapilib.models.request.EverythingRequest;
import com.kwabenaberko.newsapilib.models.response.ArticleResponse;
import com.squareup.picasso.Picasso;


import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;

public class HomeMoviesFragment extends Fragment {

    MovieDb movie = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable  ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.home_movie_page, container, false);
        Button viewAllUpcoming = view.findViewById(R.id.allUpcomingMovieButton);
        viewAllUpcoming.setOnClickListener(v -> {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mainLayout, new UpcomingMoviesFragment(),"UpcomingMoviesFragment");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        Button viewAllPopular = view.findViewById(R.id.allPopularMovieButton);
        viewAllPopular.setOnClickListener(v -> {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mainLayout, new PopularMoviesFragment(),"PopularMoviesFragment");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        Button viewAllTop = view.findViewById(R.id.allTopRatedMovieButton);
        viewAllTop.setOnClickListener(v -> {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mainLayout, new TopMoviesFragment(),"PopularMoviesFragment");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        Button viewAllNews = view.findViewById(R.id.allMovieNewsButton);
        viewAllNews.setOnClickListener(v -> {
            Bundle extras = new Bundle();
            extras.putString("Type", "Cinema");

            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mainLayout, new NewsFragment(extras),"NewsFragment");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        LinearLayoutCompat horizontalPopularMLayout = view.findViewById(R.id.horizontalPopularMovieLayout);
        LinearLayoutCompat horizontalUpcomingMLayout = view.findViewById(R.id.horizontalUpcomingMovieLayout);
        LinearLayoutCompat horizontalTopRatedMLayout = view.findViewById(R.id.horizontalTopRatedMovieLayout);
        LinearLayoutCompat horizontalNewsMovieCardLayout = view.findViewById(R.id.horizontalNewsMovieCardLayout);

        LayoutInflater inflater = LayoutInflater.from(getActivity());

        fillHorizontalUpcomingLayout(horizontalUpcomingMLayout,inflater);
        fillHorizontalPopularLayout(horizontalPopularMLayout,inflater);
        fillHorizontalTopRatedLayout(horizontalTopRatedMLayout,inflater);
        fillHorizontalNewsLayout(horizontalNewsMovieCardLayout,inflater);

    }

    private void fillHorizontalNewsLayout(LinearLayoutCompat layout, LayoutInflater inflater) {

        NewsApiClient newsApiClient = new NewsApiClient("cda09fbffad74cd5a1852e20b81ba8ee");
        newsApiClient.getEverything(
                new EverythingRequest.Builder()
                        .q("Cinema")
                        .build(),
                new NewsApiClient.ArticlesResponseCallback() {
                    @Override
                    public void onSuccess(ArticleResponse response) {
                        fillNewsLayout(layout,inflater,response);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        System.out.println(throwable.getMessage());
                    }
                }
        );
    }

    private void fillHorizontalUpcomingLayout(LinearLayoutCompat layout, LayoutInflater inflater){

        new MoviesApi(new ApiResponse() {
            @Override
            public void processFinish(Object output) {
                MoviesUtilities.upcomingMovies = (List<MovieDb>) output;
                fillLayout(layout,inflater, output,"Upcoming");
            }
        }).execute("UpcomingMovies");


    }

    private void fillHorizontalPopularLayout(LinearLayoutCompat layout, LayoutInflater inflater){
        new MoviesApi(new ApiResponse() {
            @Override
            public void processFinish(Object output) {
                MoviesUtilities.popMovies = (List<MovieDb>) output;
                fillLayout(layout,inflater, output,"Popular");
            }
        }).execute("PopMovies");
    }

    private void fillHorizontalTopRatedLayout(LinearLayoutCompat layout, LayoutInflater inflater){
        new MoviesApi(new ApiResponse() {
            @Override
            public void processFinish(Object output) {
                MoviesUtilities.topMovies = (List<MovieDb>) output;
                fillLayout(layout,inflater, output,"Top");
            }
        }).execute("TopMovies");

    }

    private void fillLayout(LinearLayoutCompat layout, LayoutInflater inflater, Object object, String type){
        List<MovieDb> res = (List<MovieDb>) object;
        for (int i = 0; i < 5;  i++){
            View card = inflater.inflate(R.layout.card_layout, layout, false);

            TextView textView = card.findViewById(R.id.title);
            textView.setText(res.get(i).getTitle());

            ImageView imageView = card.findViewById(R.id.thumbnail);

            if (res.get(i).getPosterPath() != null){
                Glide.with(getContext())
                        .load("https://image.tmdb.org/t/p/original" + res.get(i).getPosterPath())
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }

            TextView rating = card.findViewById(R.id.rating);
            rating.setText(String.format("%.1f", res.get(i).getVoteAverage()));

            final int index = i;
            card.setOnClickListener(v -> {
                Intent intent = new Intent(getContext(), DetailsActivity.class);

                new MoviesApi(new ApiResponse() {
                    @Override
                    public void processFinish(Object output) {
                        movie = (MovieDb) output;
                        Bundle extras = new Bundle();
                        extras.putSerializable("Movie", movie);
                        extras.putString("Type", type);
                        intent.putExtras(extras);

                        getContext().startActivity(intent);
                    }
                }).execute("Get dettagli Film", String.valueOf(res.get(index).getId()));
            });

            layout.addView(card);
        }
    }

    private void fillNewsLayout(LinearLayoutCompat layout, LayoutInflater inflater, ArticleResponse resIn){
        List<Article> articleList = resIn.getArticles();

        for(int i = 0; i < 5;  i++){
            View card = inflater.inflate(R.layout.news_card_layout, layout, false);

            TextView textView = card.findViewById(R.id.title);
            textView.setText(articleList.get(i).getTitle());

            ImageView imageView = card.findViewById(R.id.thumbnail);

            Picasso.get()
                    .load(articleList.get(i).getUrlToImage())
                    .resize(300,150)
                    .centerCrop()
                    .into(imageView);

            TextView description = card.findViewById(R.id.description);
            TextView source = card.findViewById(R.id.source);

            description.setText(articleList.get(i).getDescription());
            source.setText(articleList.get(i).getSource().getName());


            final int index = i;
            card.setOnClickListener(v -> {
                Uri uri = Uri.parse(articleList.get(index).getUrl());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            });

            layout.addView(card);
        }

    }
}