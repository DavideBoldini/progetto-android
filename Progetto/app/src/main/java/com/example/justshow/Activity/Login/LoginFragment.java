package com.example.justshow.Activity.Login;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.example.justshow.Activity.Home.MainActivity;
import com.example.justshow.Items.ProfileItem;
import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

public class LoginFragment extends Fragment {

    TextInputEditText emailTextInputEditText;
    TextInputEditText passwordTextInputEditText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_page,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        emailTextInputEditText = view.findViewById(R.id.emailInputEdit);
        passwordTextInputEditText = view.findViewById(R.id.passwordInputEdit);

        Button loginButton = view.findViewById(R.id.loginButton);
        Button backLoginButton = view.findViewById(R.id.backLoginButton);

        LoginViewModel loginViewModel = new ViewModelProvider((ViewModelStoreOwner) getActivity()).get(LoginViewModel.class);

        loginButton.setOnClickListener(v -> {
            String email = emailTextInputEditText.getText().toString();
            String pass = passwordTextInputEditText.getText().toString();

            if (email.equals("") || pass.equals("")){
                Toast.makeText(getContext(),"Login non valido",Toast.LENGTH_LONG).show();
                emailTextInputEditText.setText("");
                passwordTextInputEditText.setText("");
            }else {
                loginViewModel.checkLogin(email,pass).observe(getViewLifecycleOwner(), new Observer<List<ProfileItem>>() {
                    @Override
                    public void onChanged(List<ProfileItem> profileItems) {
                        if (profileItems == null || profileItems.size() == 0){
                            Toast.makeText(getContext(),"Utente non trovato",Toast.LENGTH_LONG).show();
                            emailTextInputEditText.setText("");
                            passwordTextInputEditText.setText("");
                        }else {
                            ((JustShow) getActivity().getApplication()).setUserLogged(true);
                            ((JustShow) getActivity().getApplication()).setActiveProfile(profileItems.get(0));
                            Toast.makeText(getContext(),"Accesso eseguito con successo",Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }
                });
            }
        });

        backLoginButton.setOnClickListener(v -> getActivity().finish());

    }
}
