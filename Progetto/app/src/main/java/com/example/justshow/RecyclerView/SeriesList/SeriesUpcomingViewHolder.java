package com.example.justshow.RecyclerView.SeriesList;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.justshow.R;
import com.sackcentury.shinebuttonlib.ShineButton;

public class SeriesUpcomingViewHolder extends RecyclerView.ViewHolder{

    ImageView thumbnailUpcomingTvRowImageView;
    ShineButton favoriteUpcomingTvFilterButton;
    TextView titleUpcomingTvRowTextView;
    TextView plotUpcomingTvRowTextView;
    View dividerUpcomingTvRowView;
    TextView dateUpcomingTvRowTextView;


    public SeriesUpcomingViewHolder(@NonNull View itemView) {
        super(itemView);

        thumbnailUpcomingTvRowImageView = itemView.findViewById(R.id.thumbnailUpcomingSeriesTvRow);
        favoriteUpcomingTvFilterButton = itemView.findViewById(R.id.favoriteUpcomingSeriesTvFilterButton);
        titleUpcomingTvRowTextView = itemView.findViewById(R.id.titleUpcomingSeriesTvRow);
        plotUpcomingTvRowTextView = itemView.findViewById(R.id.plotUpcomingSeriesTvRow);
        dividerUpcomingTvRowView = itemView.findViewById(R.id.plotUpcomingSeriesTvRowDivider);
        dateUpcomingTvRowTextView = itemView.findViewById(R.id.dateUpcomingSeriesTvRow);

    }

    public void loadImage(String posterPath, Activity activity){
        Glide.with(activity)
                .load(posterPath)
                .error(R.drawable.ic_baseline_image_not_supported_24)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(thumbnailUpcomingTvRowImageView);
    }
}
