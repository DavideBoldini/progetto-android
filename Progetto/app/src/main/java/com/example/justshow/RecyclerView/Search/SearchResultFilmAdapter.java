package com.example.justshow.RecyclerView.Search;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.Activity.Details.IntentDetailsCreator;
import com.example.justshow.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;

public class SearchResultFilmAdapter extends RecyclerView.Adapter<SearchResultViewHolder> {

    private final List<MovieDb> searchItemList;
    private final Activity activity;

    public SearchResultFilmAdapter(final List<MovieDb> searchItemList, final Activity activity){
        this.searchItemList = searchItemList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.result_row,parent,false);
        return new SearchResultViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultViewHolder holder, int position) {
        MovieDb currentSearchItem = searchItemList.get(position);

        holder.titleResultRowTextView.setText(currentSearchItem.getTitle());
        if (currentSearchItem.getOverview().isEmpty()){
            holder.plotResultRowTextView.setText("Nessuna trama disponibile");
        }else{
            holder.plotResultRowTextView.setText(currentSearchItem.getOverview());
        }
        holder.ratingResultRowTextView.setText(String.valueOf(currentSearchItem.getVoteAverage()));

        holder.itemView.setOnClickListener(v -> {
            IntentDetailsCreator intentCreator = new IntentDetailsCreator();
            Intent intent = intentCreator.createIntentMovies(activity, "", currentSearchItem.getId());
            activity.startActivity(intent);
        });

        if (currentSearchItem.getPosterPath() != null && !currentSearchItem.getPosterPath().equals("")){
            Picasso.get()
                    .load("https://image.tmdb.org/t/p/w185" + currentSearchItem.getPosterPath())
                    .resize(150,220)
                    .centerCrop()
                    .into(holder.thumbnailResultRowImageView);
        }

    }

    @Override
    public int getItemCount() {
        return searchItemList.size();
    }


}
