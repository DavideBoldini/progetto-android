package com.example.justshow.Utilities;

import java.util.ArrayList;
import java.util.List;

import info.movito.themoviedbapi.model.tv.TvSeries;

public class TvSeriesUtilities {

    public static List<TvSeries> upcomingTv = new ArrayList<>();
    public static List<TvSeries> topTv = new ArrayList<>();
    public static List<TvSeries> popTv = new ArrayList<>();

    public static List<TvSeries> getUpcomingTv() {
        return upcomingTv;
    }

    public static List<TvSeries> getTopTv() {
        return topTv;
    }

    public static List<TvSeries> getPopTv() {
        return popTv;
    }

}
