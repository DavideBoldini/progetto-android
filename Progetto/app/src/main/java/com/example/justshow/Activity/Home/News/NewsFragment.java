package com.example.justshow.Activity.Home.News;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.R;
import com.example.justshow.RecyclerView.News.NewsListAdapter;
import com.kwabenaberko.newsapilib.NewsApiClient;
import com.kwabenaberko.newsapilib.models.Article;
import com.kwabenaberko.newsapilib.models.request.EverythingRequest;
import com.kwabenaberko.newsapilib.models.response.ArticleResponse;

import java.util.List;

public class NewsFragment extends Fragment {

    private RecyclerView recyclerView;
    private NewsListAdapter newsListAdapter;
    private final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
    private Bundle extras;

    public NewsFragment(){}
    public NewsFragment(final Bundle extras){
        this.extras = extras;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle("Type",extras);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            extras = savedInstanceState.getBundle("Type");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.latest_news, container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();
        if (activity != null) {
            setRecyclerView(activity);
        }else {
            Log.e("NewsFragment","Activity is null");
        }
    }

    public void setRecyclerView(final Activity activity) {
        recyclerView = getView().findViewById(R.id.allNewsListRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(llm);

        String type = extras.getString("Type");

        NewsApiClient newsApiClient = new NewsApiClient("cda09fbffad74cd5a1852e20b81ba8ee");
        newsApiClient.getEverything(
                new EverythingRequest.Builder()
                        .q(type)
                        .build(),
                new NewsApiClient.ArticlesResponseCallback() {
                    @Override
                    public void onSuccess(ArticleResponse response) {
                        List<Article> articleList = response.getArticles().subList(0,15);
                        newsListAdapter = new NewsListAdapter(articleList,activity);
                        recyclerView.setAdapter(newsListAdapter);
                    }
                    @Override
                    public void onFailure(Throwable throwable) {
                        System.out.println(throwable.getMessage());
                    }
                }
        );
    }
}
