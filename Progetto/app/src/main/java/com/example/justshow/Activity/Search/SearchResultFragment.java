package com.example.justshow.Activity.Search;

import static com.example.justshow.Activity.Search.SearchResultActivity.totMovieList;
import static com.example.justshow.Activity.Search.SearchResultActivity.totTvList;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chivorn.smartmaterialspinner.SmartMaterialSpinner;
import com.example.justshow.R;
import com.example.justshow.RecyclerView.Search.SearchResultFilmAdapter;
import com.example.justshow.RecyclerView.Search.SearchResultSerieAdapter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.tv.TvSeries;

public class SearchResultFragment extends Fragment {

    private RecyclerView recyclerView;

    private ArrayList<String> sortList;
    private String sortSelected;

    private String type;

    private List<MovieDb> totMovieListFrag = new ArrayList<>();
    private List<TvSeries> totTvListFrag = new ArrayList<>();

    private SearchResultFilmAdapter searchResultFilmAdapter;
    private SearchResultSerieAdapter searchResultSerieAdapter;
    private final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
    private Bundle bundle;

    private boolean flag = false;


    public SearchResultFragment(){}

    public SearchResultFragment(final Bundle bundle){
        this.bundle = bundle;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.result_layout,container,false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initSpinner(view);
        if(bundle.getString("Type").equals("Film")){
            type = "Film";
            totMovieListFrag.addAll(totMovieList);
        }else {
            type = "TvSeries";
            totTvListFrag.addAll(totTvList);
        }


        final Activity activity = getActivity();
        if (activity != null){
            setRecyclerView();
        }else {
            Log.e("SearchResFragment","Activity is null");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setRecyclerView(){
        recyclerView = getView().findViewById(R.id.resultListRecyclerView);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(llm);

        if(type.equals("Film")){

            searchResultFilmAdapter = new SearchResultFilmAdapter(totMovieList, getActivity());
            recyclerView.setAdapter(searchResultFilmAdapter);

            TextView numRes = getView().findViewById(R.id.numberResultTextView);
            numRes.append(" " + totMovieList.size());
        }else {

            searchResultSerieAdapter = new SearchResultSerieAdapter(totTvList, getActivity());
            recyclerView.setAdapter(searchResultSerieAdapter);

            TextView numRes = getView().findViewById(R.id.numberResultTextView);
            numRes.append(" " + totTvList.size());
        }

    }


    private void initSpinner(View view){
        SmartMaterialSpinner<String> sortSpinner = view.findViewById(R.id.sortSpinner);
        sortList = new ArrayList<>();

        sortList.add("Rilevanza");
        sortList.add("Voto crescente");
        sortList.add("Voto decrescente");

        sortSpinner.setItem(sortList);
        sortSpinner.setSelection(0,false);

        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (flag){
                    sortSelected = sortList.get(position);
                    updateRecyclerView();
                }
                flag = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updateRecyclerView(){
        String type = bundle.getString("Type");
        if (type.equals("Film")){
            if (sortSelected.equals("Voto crescente")){
                totMovieListFrag.sort((o1, o2) -> Float.compare(o1.getVoteAverage(), o2.getVoteAverage()));
                searchResultFilmAdapter = new SearchResultFilmAdapter(totMovieListFrag, getActivity());
                recyclerView.setAdapter(searchResultFilmAdapter);
            }else if (sortSelected.equals("Voto decrescente")){
                totMovieListFrag.sort((o1, o2) -> Float.compare(o2.getVoteAverage(), o1.getVoteAverage()));
                searchResultFilmAdapter = new SearchResultFilmAdapter(totMovieListFrag, getActivity());
                recyclerView.setAdapter(searchResultFilmAdapter);
            }else {
                totMovieListFrag = new ArrayList<>(totMovieList);
                searchResultFilmAdapter = new SearchResultFilmAdapter(totMovieListFrag, getActivity());
                recyclerView.setAdapter(searchResultFilmAdapter);
            }

        }else {

            if (sortSelected.equals("Voto crescente")){
                totTvListFrag.sort((o1, o2) -> Float.compare(o1.getVoteAverage(), o2.getVoteAverage()));
                searchResultSerieAdapter = new SearchResultSerieAdapter(totTvListFrag, getActivity());
                recyclerView.setAdapter(searchResultSerieAdapter);
            }else if (sortSelected.equals("Voto decrescente")){
                totTvListFrag.sort(new Comparator<TvSeries>() {
                    @Override
                    public int compare(TvSeries o1, TvSeries o2) {
                        return Float.compare(o2.getVoteAverage(), o1.getVoteAverage());
                    }
                });
                searchResultSerieAdapter = new SearchResultSerieAdapter(totTvListFrag, getActivity());
                recyclerView.setAdapter(searchResultSerieAdapter);
            }else {
                totTvListFrag = new ArrayList<>(totTvList);
                searchResultSerieAdapter = new SearchResultSerieAdapter(totTvListFrag, getActivity());
                recyclerView.setAdapter(searchResultSerieAdapter);
            }
        }
    }
}
