package com.example.justshow.Activity.Search;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.example.justshow.Utilities.ApiResponse;
import com.example.justshow.Utilities.MoviesApi;
import com.example.justshow.Utilities.TvSeriesApi;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.slider.RangeSlider;

import net.steamcrafted.loadtoast.LoadToast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.tv.TvSeries;

public class SearchFragment extends Fragment {

    List<MovieDb> resFilmList = null;
    List<TvSeries> resSerieList = null;
    List<String> lastSearchList = null;
    LoadToast lt;
    BottomNavigationView bottomNavigationView;

    private MoviesApi moviesApi;
    private TvSeriesApi tvSeriesApi;



    @Override
    public void onStop() {
        super.onStop();
        if (moviesApi != null && moviesApi.getStatus() == AsyncTask.Status.RUNNING){
            moviesApi.cancel(true);
        }else if (tvSeriesApi != null && tvSeriesApi.getStatus() == AsyncTask.Status.RUNNING){
            tvSeriesApi.cancel(true);
        }
        if (lt != null){
            lt.hide();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.search_page, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        lastSearchList = ((JustShow) getActivity().getApplication()).getLastSearchList();

        bottomNavigationView = getActivity().findViewById(R.id.bottomAppBar);
        bottomNavigationView.getMenu().findItem(R.id.search_bottom).setChecked(true);

        AutoCompleteTextView titleEditText = view.findViewById(R.id.titleSearchInputEditText);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, lastSearchList);
        titleEditText.setThreshold(1);
        titleEditText.setAdapter(adapter);


        RadioGroup typeGroup = view.findViewById(R.id.radioType);
        typeGroup.check(R.id.radioFilm);
        ChipGroup chipGroup = view.findViewById(R.id.genreChipGroup);

        RangeSlider voteRangeSlider = view.findViewById(R.id.voteRangeSlider);

        NumberPicker daYearPicker = view.findViewById(R.id.daYearNumberPicker);
        NumberPicker aYearPicker = view.findViewById(R.id.aYearNumberPicker);

        daYearPicker.setMaxValue(2021);
        daYearPicker.setMinValue(1920);

        aYearPicker.setMaxValue(2021);
        aYearPicker.setMinValue(1920);

        aYearPicker.setValue(2021);

        Button searchButton = view.findViewById(R.id.searchButton);
        Button cancelButton = view.findViewById(R.id.cancelSearchButton);

        cancelButton.setOnClickListener(v -> {
            titleEditText.setText(null);
            typeGroup.check(R.id.radioFilm);
            chipGroup.clearCheck();
            daYearPicker.setValue(1920);
            aYearPicker.setValue(2021);
        });

        searchButton.setOnClickListener(v -> {

            String title = titleEditText.getText().toString();
            if (title.equals("")){
                title = null;
            }else {
                lastSearchList.add(title);
                checkList();
            }

            int selectedType = typeGroup.getCheckedRadioButtonId();
            View type = typeGroup.findViewById(selectedType);
            int radioId = typeGroup.indexOfChild(type);
            RadioButton rType = (RadioButton) typeGroup.getChildAt(radioId);
            String text = (String) rType.getText();

            String azione = null;
            String avventura = null;
            String horror = null;
            String thriller = null;
            String romantico = null;
            String drammatico = null;
            String commedia = null;
            String animazione = null;
            String fantascienza = null;
            String reality = null;
            String mistero = null;


            List<Integer> ids = new ArrayList<>(chipGroup.getCheckedChipIds());
            List<String> chipsSelected = new ArrayList<>();
            for (Integer id : ids){
                Chip chip = chipGroup.findViewById(id);
                chipsSelected.add(chip.getText().toString());
            }

            if (title == null && chipsSelected.size() == 0){
                Toast.makeText(getContext(),"Selezionare almeno un genere",Toast.LENGTH_SHORT).show();
                return;
            }

            lt = new LoadToast(getActivity());
            lt.setText("Ricerca in corso...");
            lt.setTranslationY(1000);
            lt.show();

            if (chipsSelected.contains("Azione")){
                azione = "28";
            }
            if (chipsSelected.contains("Avventura")){
                avventura = "12";
            }
            if (chipsSelected.contains("Horror")){
                horror = "27";
            }
            if (chipsSelected.contains("Thriller")){
                thriller = "53";
            }
            if (chipsSelected.contains("Romantico")){
                romantico = "10749";
            }
            if (chipsSelected.contains("Drammatico")){
                drammatico = "18";
            }
            if (chipsSelected.contains("Commedia")){
                commedia = "35";
            }
            if (chipsSelected.contains("Animazione")){
                animazione = "16";
            }
            if (chipsSelected.contains("Fantascienza")){
                fantascienza = "878";
            }
            if (chipsSelected.contains("Reality")){
                reality = "10764";
            }
            if (chipsSelected.contains("Mistero")){
                mistero = "9648";
            }

            List<Float> rangeVal = voteRangeSlider.getValues();
            String minVote = Collections.min(rangeVal).toString();
            String maxVote = Collections.max(rangeVal).toString();

            String daYear = String.valueOf(daYearPicker.getValue());
            String aYear = String.valueOf(aYearPicker.getValue());

            if (text.equals("Film")){

                moviesApi = new MoviesApi(new ApiResponse() {
                    @Override
                    public void processFinish(Object output) {
                        lt.success();
                        resFilmList = (List<MovieDb>) output;
                        Intent intent = new Intent(getContext(), SearchResultActivity.class);

                        SearchResultActivity.totMovieList = resFilmList;
                        Bundle extras = new Bundle();
                        extras.putString("Type", "Film");
                        intent.putExtras(extras);
                        getContext().startActivity(intent);
                    }
                });

                moviesApi.execute("Cerca Film dettagliata",title,azione,avventura,horror,thriller,romantico,
                        drammatico,commedia,animazione,fantascienza,reality,mistero,minVote,maxVote,daYear,aYear,"0");

            } else {
                if (azione != null || avventura != null){
                    azione = "10759";
                }
                if (fantascienza != null){
                    fantascienza = "10765";
                }
                if (thriller != null){
                    thriller = "80";
                }
                if (horror != null){
                    horror = "9648";
                }

                tvSeriesApi = new TvSeriesApi(new ApiResponse() {
                    @Override
                    public void processFinish(Object output) {
                        lt.success();
                        resSerieList = (List<TvSeries>) output;
                        Intent intent = new Intent(getContext(), SearchResultActivity.class);

                        SearchResultActivity.totTvList = resSerieList;
                        Bundle extras = new Bundle();
                        extras.putString("Type", "Serie");
                        intent.putExtras(extras);
                        getContext().startActivity(intent);
                    }
                });
                tvSeriesApi.execute("Cerca Serie dettagliata",title,azione,avventura,horror,thriller,romantico,
                        drammatico,commedia,animazione,fantascienza,reality,mistero,minVote,maxVote,daYear,aYear);

            }
        });

    }

    private void checkList(){
        if (lastSearchList.size() > 10){
            lastSearchList.remove(0);
        }
        ((JustShow) getActivity().getApplication()).setLastSearchList(lastSearchList);
    }

}
