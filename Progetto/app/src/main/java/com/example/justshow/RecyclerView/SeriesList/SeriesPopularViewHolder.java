package com.example.justshow.RecyclerView.SeriesList;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.justshow.R;
import com.sackcentury.shinebuttonlib.ShineButton;

public class SeriesPopularViewHolder extends RecyclerView.ViewHolder{

    ImageView thumbnailPopularTvRowImageView;
    ShineButton favoritePopularTvFilterButton;
    TextView titlePopularTvRowTextView;
    TextView plotPopularTvRowTextView;
    View dividerPopularTvRowView;
    TextView popularityPopularTvRowTextView;

    public SeriesPopularViewHolder(@NonNull View itemView) {
        super(itemView);

        thumbnailPopularTvRowImageView = itemView.findViewById(R.id.thumbnailPopularSeriesTvRow);
        favoritePopularTvFilterButton = itemView.findViewById(R.id.favoritePopularSeriesTvFilterButton);
        titlePopularTvRowTextView = itemView.findViewById(R.id.titlePopularSeriesTvRow);
        plotPopularTvRowTextView = itemView.findViewById(R.id.plotPopularSeriesTvRow);
        dividerPopularTvRowView = itemView.findViewById(R.id.plotPopularSeriesTvRowDivider);
        popularityPopularTvRowTextView = itemView.findViewById(R.id.popularityPopularSeriesTvRow);
    }

    public void loadImage(String posterPath, Activity activity){
        Glide.with(activity)
                .load(posterPath)
                .error(R.drawable.ic_baseline_image_not_supported_24)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(thumbnailPopularTvRowImageView);
    }
}
