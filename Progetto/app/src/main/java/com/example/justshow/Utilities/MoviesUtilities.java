package com.example.justshow.Utilities;

import java.util.ArrayList;
import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;

public class MoviesUtilities {

    public static List<MovieDb> upcomingMovies = new ArrayList<>();
    public static List<MovieDb> topMovies = new ArrayList<>();
    public static List<MovieDb> popMovies = new ArrayList<>();

    public static List<MovieDb> getUpcomingMovies() {
        return upcomingMovies;
    }

    public static List<MovieDb> getTopMovies() {
        return topMovies;
    }

    public static List<MovieDb> getPopMovies() {
        return popMovies;
    }
}
