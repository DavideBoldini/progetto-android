package com.example.justshow.RecyclerView.MoviesList;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.justshow.R;
import com.sackcentury.shinebuttonlib.ShineButton;

public class MoviesPopularViewHolder extends RecyclerView.ViewHolder{

    ImageView thumbnailPopularMoviesRowImageView;
    ShineButton favoritePopularMoviesFilterButton;
    TextView titlePopularMoviesRowTextView;
    TextView plotPopularMoviesRowTextView;
    View dividerPopularMoviesRowView;
    TextView popularityPopularMoviesRowTextView;

    public MoviesPopularViewHolder(@NonNull View itemView) {
        super(itemView);

        thumbnailPopularMoviesRowImageView = itemView.findViewById(R.id.thumbnailPopularMoviesRow);
        favoritePopularMoviesFilterButton = itemView.findViewById(R.id.favoritePopularMoviesFilterButton);
        titlePopularMoviesRowTextView = itemView.findViewById(R.id.titlePopularMoviesRow);
        plotPopularMoviesRowTextView = itemView.findViewById(R.id.plotPopularMoviesRow);
        dividerPopularMoviesRowView = itemView.findViewById(R.id.plotPopularMoviesRowDivider);
        popularityPopularMoviesRowTextView = itemView.findViewById(R.id.popularityPopularMoviesRow);
    }

    public void loadImage(String posterPath, Activity activity){
        Glide.with(activity)
                .load(posterPath)
                .error(R.drawable.ic_baseline_image_not_supported_24)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(thumbnailPopularMoviesRowImageView);
    }
}
