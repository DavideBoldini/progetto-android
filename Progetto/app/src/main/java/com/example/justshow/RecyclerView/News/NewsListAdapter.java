package com.example.justshow.RecyclerView.News;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.justshow.R;
import com.kwabenaberko.newsapilib.models.Article;

import java.util.List;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListViewHolder> {

    private final List<Article> articleList;
    private final Activity activity;

    public NewsListAdapter(List<Article> articleList, Activity activity){
        this.articleList = articleList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public NewsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_row,parent,false);
        return new NewsListViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsListViewHolder holder, int position) {
        Article currentArticleItem = articleList.get(position);

        holder.title.setText(currentArticleItem.getTitle());
        holder.description.setText(currentArticleItem.getDescription());
        holder.source.setText(currentArticleItem.getSource().getName());


        Glide.with(activity)
                .load(currentArticleItem.getUrlToImage())
                .centerInside()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(holder.thumbnail);

        holder.itemView.setOnClickListener(v ->{
            Uri uri = Uri.parse(currentArticleItem.getUrl());
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            activity.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }
}
