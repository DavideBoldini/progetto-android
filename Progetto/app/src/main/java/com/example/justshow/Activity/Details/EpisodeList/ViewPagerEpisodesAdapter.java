package com.example.justshow.Activity.Details.EpisodeList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ViewPagerEpisodesAdapter extends FragmentStateAdapter {

    private final int numSeasons;
    private final String serie;
    public ViewPagerEpisodesAdapter(@NonNull FragmentActivity fragmentActivity, int numSeasons, String serie) {
        super(fragmentActivity);
        this.numSeasons = numSeasons;
        this.serie = serie;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return new EpisodeListFragment(serie,position+1);
    }

    @Override
    public int getItemCount() {
        return numSeasons;
    }
}
