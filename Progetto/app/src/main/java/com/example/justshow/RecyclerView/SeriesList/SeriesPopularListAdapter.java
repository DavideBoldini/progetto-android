package com.example.justshow.RecyclerView.SeriesList;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.Activity.Details.IntentDetailsCreator;
import com.example.justshow.Activity.Home.Profile.ProfileViewModel;
import com.example.justshow.Items.ProfileItem;
import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.List;

import info.movito.themoviedbapi.model.tv.TvSeries;

public class SeriesPopularListAdapter extends RecyclerView.Adapter<SeriesPopularViewHolder>{

    private final List<TvSeries> seriesList;
    private final Activity activity;
    ShineButton favorite;

    public SeriesPopularListAdapter(final List<TvSeries> seriesList, final Activity activity) {
        this.seriesList = seriesList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public SeriesPopularViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.seriestv_popular_row,parent,false);

        favorite = layoutView.findViewById(R.id.favoritePopularSeriesTvFilterButton);

        return new SeriesPopularViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull SeriesPopularViewHolder holder, int position) {
        TvSeries currentPopularSeriesItem = seriesList.get(position);

        if (currentPopularSeriesItem.getPosterPath() != null && !currentPopularSeriesItem.getPosterPath().equals("")){
            holder.loadImage("https://image.tmdb.org/t/p/original" + currentPopularSeriesItem.getPosterPath(), activity);
        }
        holder.titlePopularTvRowTextView.setText(currentPopularSeriesItem.getName());

        if (currentPopularSeriesItem.getOverview().isEmpty()){
            holder.plotPopularTvRowTextView.setText("Nessuna trama disponibile");
        }else{
            holder.plotPopularTvRowTextView.setText(currentPopularSeriesItem.getOverview());
        }

        holder.popularityPopularTvRowTextView.setText(String.valueOf(currentPopularSeriesItem.getPopularity()));

        holder.itemView.setOnClickListener(v -> {
            IntentDetailsCreator intentCreator = new IntentDetailsCreator();
            Intent intent = intentCreator.createIntentSeries(activity, "Popular", currentPopularSeriesItem.getId());
            activity.startActivity(intent);
        });

        ProfileViewModel profileViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(ProfileViewModel.class);
        ProfileItem activeProfile = ((JustShow) activity.getApplication()).getActiveProfile();

        favorite.setChecked(activeProfile != null && activeProfile.getFavoriteTvSeriesList().contains(currentPopularSeriesItem));

        favorite.setOnClickListener(v -> {
            if (activeProfile != null) {
                List<TvSeries> newList = activeProfile.getFavoriteTvSeriesList();
                if (newList.contains(currentPopularSeriesItem)){
                    newList.remove(currentPopularSeriesItem);
                }else {
                    newList.add(currentPopularSeriesItem);
                }
                activeProfile.setFavoriteTvSeriesList(newList);
                profileViewModel.updateFavoriteList(activeProfile.getFavoriteMovieList(),activeProfile.getFavoriteTvSeriesList(),activeProfile.getId());
            }else{
                favorite.setChecked(false);
                Toast.makeText(activity,"Necessario effettuare il login", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return seriesList.size();
    }
}
