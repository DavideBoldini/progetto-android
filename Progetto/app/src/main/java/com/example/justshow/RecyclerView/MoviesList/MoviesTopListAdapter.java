package com.example.justshow.RecyclerView.MoviesList;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.example.justshow.Activity.Details.IntentDetailsCreator;
import com.example.justshow.Activity.Home.Profile.ProfileViewModel;
import com.example.justshow.Items.ProfileItem;
import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;

public class MoviesTopListAdapter extends RecyclerView.Adapter<MoviesTopViewHolder>{

    private final List<MovieDb> moviesList;
    private final Activity activity;
    ShineButton favorite;

    public MoviesTopListAdapter(List<MovieDb> moviesList, Activity activity){
        this.moviesList = moviesList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MoviesTopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movies_top_row, parent,false);

        favorite = layoutView.findViewById(R.id.favoriteTopMoviesFilterButton);

        return new MoviesTopViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesTopViewHolder holder, int position) {
        MovieDb currentTopMovieItem = moviesList.get(position);

        holder.loadImage("https://image.tmdb.org/t/p/original" + currentTopMovieItem.getPosterPath(), activity);
        holder.titleTopMoviesRowTextView.setText(currentTopMovieItem.getTitle());

        if (currentTopMovieItem.getOverview().isEmpty()){
            holder.plotTopMoviesRowTextView.setText("Nessuna trama disponibile");
        }else{
            holder.plotTopMoviesRowTextView.setText(currentTopMovieItem.getOverview());
        }

        holder.ratingTopMoviesRowTextView.setText(String.valueOf(currentTopMovieItem.getVoteAverage()));

        holder.itemView.setOnClickListener(v -> {
            IntentDetailsCreator intentCreator = new IntentDetailsCreator();
            Intent intent = intentCreator.createIntentMovies(activity, "Top", currentTopMovieItem.getId());
            activity.startActivity(intent);
        });

        ProfileViewModel profileViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(ProfileViewModel.class);
        ProfileItem activeProfile = ((JustShow) activity.getApplication()).getActiveProfile();

        favorite.setChecked(activeProfile != null && activeProfile.getFavoriteMovieList().contains(currentTopMovieItem));

        favorite.setOnClickListener(v -> {
            if (activeProfile != null) {
                    List<MovieDb> newList = activeProfile.getFavoriteMovieList();
                    if (newList.contains(currentTopMovieItem)){
                        newList.remove(currentTopMovieItem);
                    }else {
                        newList.add(currentTopMovieItem);
                    }
                    activeProfile.setFavoriteMovieList(newList);
                    profileViewModel.updateFavoriteList(activeProfile.getFavoriteMovieList(),activeProfile.getFavoriteTvSeriesList(),activeProfile.getId());
                }else {
                favorite.setChecked(false);
                Toast.makeText(activity,"Necessario effettuare il login", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
