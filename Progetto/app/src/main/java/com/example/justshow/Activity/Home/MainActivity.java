package com.example.justshow.Activity.Home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.justshow.Activity.Home.Profile.ProfileFragment;
import com.example.justshow.Activity.Home.Profile.WelcomeActivity;
import com.example.justshow.Activity.Search.SearchFragment;
import com.example.justshow.Items.ProfileItem;
import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity implements NavigationBarView.OnItemSelectedListener{

    BottomNavigationView bottomNavigationView;
    ViewPager2 viewPager2;
    ViewPagerAdapter adapter;
    boolean userLogged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userLogged = ((JustShow) this.getApplication()).getUserLogged();
        bottomNavigationView = findViewById(R.id.bottomAppBar);
        bottomNavigationView.setOnItemSelectedListener(this);

        if (userLogged){
            ProfileItem activeProfile = ((JustShow) getApplication()).getActiveProfile();
            Menu menu = bottomNavigationView.getMenu();
            menu.findItem(R.id.profile_bottom).setTitle(activeProfile.getNome());
        }

        //get elements
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.requestDisallowInterceptTouchEvent(true);
        viewPager2 = findViewById(R.id.viewPager);

        adapter = new ViewPagerAdapter(this);
        viewPager2.setAdapter(adapter);

        viewPager2.setUserInputEnabled(false);

        new TabLayoutMediator(tabLayout, viewPager2, (tab, position) -> {
            if (position == 1){
                tab.setText("Tv");
            }else {
                tab.setText("Film");
            }
        }).attach();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();

        int id = item.getItemId();
        Fragment fragment = null;
        String tag = "";

        switch (id){
            case R.id.home_bottom:
                if (getSupportFragmentManager().findFragmentByTag("SearchFragment") != null || getSupportFragmentManager().findFragmentByTag("WelcomeFragment") != null
                        || getSupportFragmentManager().findFragmentByTag("ProfileFragment") != null) {
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    this.startActivity(intent);
                    return true;
                }
                return true;

            case R.id.search_bottom:
                fragment = new SearchFragment();
                tag = "SearchFragment";
                break;
            case R.id.profile_bottom:
                if (!userLogged){
                    Intent intent = new Intent(this, WelcomeActivity.class);
                    this.startActivity(intent);
                    return true;
                }else {
                    fragment = new ProfileFragment();
                    tag = "ProfileFragment";
                    break;
                }
        }
        fragmentTransaction.replace(R.id.principalView, fragment, tag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        return true;
    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }

        Fragment movie = getSupportFragmentManager().findFragmentByTag("f0");
        Fragment tv = getSupportFragmentManager().findFragmentByTag("f1");

        if (movie.isVisible() || tv.isVisible()){
            bottomNavigationView.getMenu().findItem(R.id.home_bottom).setChecked(true);
        }

    }

}