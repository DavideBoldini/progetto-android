package com.example.justshow.Activity.Details;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.justshow.Activity.Details.EpisodeList.EpisodeListActivity;
import com.example.justshow.Activity.Home.Profile.ProfileViewModel;
import com.example.justshow.Items.ProfileItem;
import com.example.justshow.JustShow;
import com.example.justshow.R;
import com.example.justshow.Utilities.MoviesApi;
import com.example.justshow.Utilities.ApiResponse;
import com.example.justshow.Utilities.TvSeriesApi;
import com.google.android.material.button.MaterialButton;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.PlayerUiController;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import info.movito.themoviedbapi.model.Credits;
import info.movito.themoviedbapi.model.Genre;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.MovieImages;
import info.movito.themoviedbapi.model.Reviews;
import info.movito.themoviedbapi.model.Video;
import info.movito.themoviedbapi.model.people.PersonCrew;
import info.movito.themoviedbapi.model.tv.TvSeries;

public class DetailsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((DetailsActivity)getActivity()).uncheckAllItems();
        return inflater.inflate(R.layout.details_page,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        ProfileItem activeProfile = ((JustShow) getActivity().getApplication()).getActiveProfile();

        Intent intent = getActivity().getIntent();

        ShineButton favorite = view.findViewById(R.id.favoriteDetailsFilterButton);
        MaterialButton watchlistButton = view.findViewById(R.id.addWatchlistDetailsButton);

        TextView title = view.findViewById(R.id.titleDetails);
        LottieAnimationView thumbnail = view.findViewById(R.id.thumbnailDetails);
        TextView type = view.findViewById(R.id.typeDetails);
        TextView plot = view.findViewById(R.id.plotDetails);
        TextView rating = view.findViewById(R.id.ratingDetails);
        LottieAnimationView share = view.findViewById(R.id.shareAnimationView);
        Button episodes = view.findViewById(R.id.episodeDetailsButton);

        LinearLayoutCompat horizontalGalleryDetailsLayout = view.findViewById(R.id.horizontalGalleryDetailsLayout);
        LinearLayoutCompat horizontalVideoDetailsLayout = view.findViewById(R.id.horizontalVideoDetailsLayout);
        LinearLayoutCompat horizontalCastDetailsLayout = view.findViewById(R.id.horizontalCastDetailsLayout);
        LinearLayoutCompat horizontalDirectorDetailsLayout = view.findViewById(R.id.horizontalDirectorDetailsLayout);

        TextView genre = view.findViewById(R.id.detailsGenreTextView);
        TextView releaseDate = view.findViewById(R.id.releaseDateDetailsTextView);
        TextView originalLang = view.findViewById(R.id.languageDetailsTextView);
        TextView budget = view.findViewById(R.id.budgetDetailsTextView);
        TextView revenue = view.findViewById(R.id.revenueDetailsTextView);

        TextView episodeRuntime = view.findViewById(R.id.episodeRuntimeDetailsTextView);
        View episodeRuntimeDivider = view.findViewById(R.id.episodeRuntimeDetailsDivider);

        View budgetDivider = view.findViewById(R.id.budgetDetailsDivider);
        View revenueDivider = view.findViewById(R.id.revenueDetailsDivider);

        LinearLayoutCompat horizontalReccomendationDetailsLayout = view.findViewById(R.id.horizontalReccomendationDetailsLayout);
        LinearLayoutCompat horizontalSimilarDetailsLayout = view.findViewById(R.id.horizontalSimilarDetailsLayout);
        LinearLayoutCompat reviewDetailsLayout = view.findViewById(R.id.reviewDetailsLayout);

        ProfileViewModel profileViewModel = new ViewModelProvider((ViewModelStoreOwner) getActivity()).get(ProfileViewModel.class);

        Bundle extras = intent.getExtras();

        share.setOnClickListener(v -> {
            String id = "";
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            if (extras.getSerializable("Tv") != null){
                TvSeries tv = (TvSeries) intent.getSerializableExtra("Tv");
                id = String.valueOf(tv.getId());
                sendIntent.putExtra(Intent.EXTRA_TEXT, "https://www.themoviedb.org/tv/" + id);
            }else {
                MovieDb movie = (MovieDb) intent.getSerializableExtra("Movie");
                id = String.valueOf(movie.getId());
                sendIntent.putExtra(Intent.EXTRA_TEXT, "https://www.themoviedb.org/movie/" + id);
            }

            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);
        });


        if(extras.getSerializable("Tv") != null){
            TvSeries tv = (TvSeries) intent.getSerializableExtra("Tv");

            if (tv.getRecommendations().getResults() != null){
                fillRaccomandationTv(horizontalReccomendationDetailsLayout, tv.getRecommendations().getResults());
            }
            if (tv.getVideos() != null){
                fillVideos(horizontalVideoDetailsLayout,tv.getVideos());
            }


            new TvSeriesApi(new ApiResponse() {
                @Override
                public void processFinish(Object output) {
                    List<TvSeries> resList = (List<TvSeries>) output;
                    if (resList != null && resList.size() != 0){
                        fillSimilarSeries(horizontalSimilarDetailsLayout,resList);
                    }
                }
            }).execute("Get Similar Serie", String.valueOf(tv.getId()));

            if (activeProfile != null && activeProfile.getFavoriteTvSeriesList().contains(tv)){
                favorite.setChecked(true);
            }else {
                favorite.setChecked(false);
            }

            episodes.setOnClickListener(v -> {

                Intent intent1 = new Intent(getContext(), EpisodeListActivity.class);
                Bundle extras1 = new Bundle();
                extras1.putString("Title", tv.getName());
                extras1.putInt("NumSeason", tv.getNumberOfSeasons());
                intent1.putExtras(extras1);

                getContext().startActivity(intent1);
            });

            new TvSeriesApi(new ApiResponse() {
                @Override
                public void processFinish(Object output) {
                    fillGallery(horizontalGalleryDetailsLayout, (MovieImages) output);
                }
            }).execute("Get Immagini Serie", tv.getName());

            title.setText(tv.getName());
            fillPicassoImg(tv.getPosterPath(),thumbnail);

            if(tv.getNumberOfSeasons() == 1){
                type.setText("Serie TV | " + tv.getNumberOfSeasons() + " stagione | " + tv.getNumberOfEpisodes() + " episodi");
            }else{
                type.setText("Serie TV | " + tv.getNumberOfSeasons() + " stagioni | " + tv.getNumberOfEpisodes() + " episodi");
            }

            if (tv.getOverview() == null || tv.getOverview().equals("")) {
                plot.setText("Nessuna trama disponibile");
            }else{
                plot.setText(tv.getOverview());
            }

            rating.append(" " + tv.getVoteAverage());

            fillCast(horizontalCastDetailsLayout, tv.getCredits());
            fillDirector(horizontalDirectorDetailsLayout, tv.getCredits(),"Serie TV");
            fillGenre(genre,tv);

            if (tv.getFirstAirDate() != null){
                releaseDate.append(" " + tv.getFirstAirDate());
            }else {
                releaseDate.append(" Nessuna informazione");
            }

            if (tv.getEpisodeRuntime().size() == 1){
                episodeRuntime.append(" " + tv.getEpisodeRuntime().get(0) + " minuti");
            }else if (tv.getEpisodeRuntime().size() > 1){
                episodeRuntime.append(" " + Collections.min(tv.getEpisodeRuntime()) + "- " + Collections.max(tv.getEpisodeRuntime()) + " minuti");
            }else {
                episodeRuntime.append(" Nessuna informazione");
            }

            if (tv.getOriginCountry().size() != 0){
                originalLang.setText("Paese origine: " + tv.getOriginCountry().get(0));
            }else {
                originalLang.setText("Paese origine: Nessuna informazione");
            }

            budget.setVisibility(View.GONE);
            budgetDivider.setVisibility(View.GONE);

            revenue.setVisibility(View.GONE);
            revenueDivider.setVisibility(View.GONE);


            new TvSeriesApi(new ApiResponse() {
                @Override
                public void processFinish(Object output) {
                    fillReviews(reviewDetailsLayout, output);
                }
            }).execute("Get Review Serie", tv.getName());

            if (activeProfile != null && activeProfile.getWatchlist().containsKey(String.valueOf(tv.getId()))){
                watchlistButton.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.darkGrey));
                watchlistButton.setTextColor(ContextCompat.getColor(getContext(),R.color.lightPurple));
                watchlistButton.setIcon(ContextCompat.getDrawable(getContext(),R.drawable.ic_baseline_remove_24));
                watchlistButton.setIconTint(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.lightPurple)));
                watchlistButton.setText("Rimuovi da watchlist");
            }else{
                watchlistButton.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.lightPurple));
                watchlistButton.setTextColor(ContextCompat.getColor(getContext(),R.color.darkGrey));
                watchlistButton.setIcon(ContextCompat.getDrawable(getContext(),R.drawable.ic_baseline_add_24));
                watchlistButton.setIconTint(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.darkGrey)));
                watchlistButton.setText("Aggiungi a watchlist");
            }

        }else{
            MovieDb movie = (MovieDb) intent.getSerializableExtra("Movie");

            if (movie.getVideos() != null){
                fillVideos(horizontalVideoDetailsLayout,movie.getVideos());
            }
            if (movie.getRecommendations() != null){
                fillRaccomandationMovie(horizontalReccomendationDetailsLayout, movie.getRecommendations());
            }
            if (movie.getSimilarMovies() != null){
                fillSimilarMovie(horizontalSimilarDetailsLayout, movie.getSimilarMovies());
            }


            if (activeProfile != null && activeProfile.getFavoriteMovieList().contains(movie)){
                favorite.setChecked(true);
            }else {
                favorite.setChecked(false);
            }

            new MoviesApi(new ApiResponse() {
                @Override
                public void processFinish(Object output) {
                    fillGallery(horizontalGalleryDetailsLayout, (MovieImages) output);
                }
            }).execute("Get Immagini Film", movie.getTitle());

            episodes.setVisibility(View.GONE);
            title.setText(movie.getTitle());
            fillPicassoImg(movie.getPosterPath(),thumbnail);

            type.setText("Film | " + movie.getRuntime() + " minuti");
            if (movie.getOverview() == null || movie.getOverview().equals("")) {
                plot.setText("Nessuna trama disponibile");
            }else{
                plot.setText(movie.getOverview());
            }

            rating.append(" " + movie.getVoteAverage());

            fillCast(horizontalCastDetailsLayout, movie.getCredits());
            fillDirector(horizontalDirectorDetailsLayout, movie.getCredits(), "Film");
            fillGenre(genre,movie);

            releaseDate.append(" " + movie.getReleaseDate());
            originalLang.append(" " + movie.getOriginalLanguage());
            budget.append(" " + movie.getBudget() + " $");
            revenue.append(" " + movie.getRevenue() + " $");

            episodeRuntime.setVisibility(View.GONE);
            episodeRuntimeDivider.setVisibility(View.GONE);

                new MoviesApi(new ApiResponse() {
                    @Override
                    public void processFinish(Object output) {
                        fillReviews(reviewDetailsLayout, output);
                    }
                }).execute("Get Review Film", movie.getTitle());

            if (activeProfile != null && activeProfile.getWatchlist().containsKey(String.valueOf(movie.getId()))){
                watchlistButton.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.darkGrey));
                watchlistButton.setTextColor(ContextCompat.getColor(getContext(),R.color.lightPurple));
                watchlistButton.setIcon(ContextCompat.getDrawable(getContext(),R.drawable.ic_baseline_remove_24));
                watchlistButton.setIconTint(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.lightPurple)));
                watchlistButton.setText("Rimuovi da watchlist");
            }else{
                watchlistButton.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.lightPurple));
                watchlistButton.setTextColor(ContextCompat.getColor(getContext(),R.color.darkGrey));
                watchlistButton.setIcon(ContextCompat.getDrawable(getContext(),R.drawable.ic_baseline_add_24));
                watchlistButton.setIconTint(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.darkGrey)));
                watchlistButton.setText("Aggiungi a watchlist");
            }

        }

        favorite.setOnClickListener(v -> {

            if (activeProfile != null) {
                if (extras.getSerializable("Tv") != null){
                    TvSeries tv = (TvSeries) intent.getSerializableExtra("Tv");
                    List<TvSeries> newList = activeProfile.getFavoriteTvSeriesList();
                    if (newList.contains(tv)){
                        newList.remove(tv);
                    }else {
                        newList.add(tv);
                    }
                    activeProfile.setFavoriteTvSeriesList(newList);
                }else {
                    MovieDb movie = (MovieDb) intent.getSerializableExtra("Movie");
                    List<MovieDb> newList = activeProfile.getFavoriteMovieList();
                    if (newList.contains(movie)){
                        newList.remove(movie);
                    }else {
                        newList.add(movie);
                    }
                    activeProfile.setFavoriteMovieList(newList);
                }
                profileViewModel.updateFavoriteList(activeProfile.getFavoriteMovieList(),activeProfile.getFavoriteTvSeriesList(),activeProfile.getId());
            }else{
                favorite.setChecked(false);
                Toast.makeText(getContext(),"Necessario effettuare il login", Toast.LENGTH_SHORT).show();
            }
        });

        watchlistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activeProfile != null) {

                    HashMap<String,String> watchlistProfile = activeProfile.getWatchlist();
                    String idElem;
                    String value;
                    if (extras.getSerializable("Tv") != null){
                        idElem = String.valueOf(((TvSeries) extras.getSerializable("Tv")).getId());
                        value = "Tv";
                    }else {
                        idElem = String.valueOf(((MovieDb) extras.getSerializable("Movie")).getId());
                        value = "Movie";
                    }
                    if (watchlistProfile.containsKey(idElem)){
                        watchlistProfile.remove(idElem);
                        watchlistButton.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.lightPurple));
                        watchlistButton.setTextColor(ContextCompat.getColor(getContext(),R.color.darkGrey));
                        watchlistButton.setIcon(ContextCompat.getDrawable(getContext(),R.drawable.ic_baseline_add_24));
                        watchlistButton.setIconTint(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.darkGrey)));
                        watchlistButton.setText("Aggiungi a watchlist");

                    }else {
                        watchlistProfile.put(idElem,value);
                        watchlistButton.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.darkGrey));
                        watchlistButton.setTextColor(ContextCompat.getColor(getContext(),R.color.lightPurple));
                        watchlistButton.setIcon(ContextCompat.getDrawable(getContext(),R.drawable.ic_baseline_remove_24));
                        watchlistButton.setIconTint(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.lightPurple)));
                        watchlistButton.setText("Rimuovi da watchlist");
                    }
                    activeProfile.setWatchlist(watchlistProfile);
                    profileViewModel.updateWatchlist(activeProfile.getWatchlist(),activeProfile.getId());
                }else {
                    Toast.makeText(getContext(),"Necessario effettuare il login", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void fillGallery(LinearLayoutCompat layout, MovieImages gallery){
        for (int i = 0; i< gallery.getBackdrops().size(); i++){
            if (i == 10){
                break;
            }
            ImageView img = new ImageView(this.getContext());
            Picasso.get()
                    .load("https://image.tmdb.org/t/p/original" + gallery.getBackdrops().get(i).getFilePath())
                    .resize(850,500)
                    .into(img);
            layout.addView(img);
        }
    }

    private void fillCast(LinearLayoutCompat layout, Object res){
        Credits cred = (Credits) res;
        for (int i = 0; i< cred.getCast().size(); i++){
            if (i == 30){
                break;
            }
            View card = LayoutInflater.from(getActivity()).inflate(R.layout.card_cast_layout, layout, false);

            TextView textViewTitle = card.findViewById(R.id.title);
            textViewTitle.setText(cred.getCast().get(i).getName());

            TextView textViewRole = card.findViewById(R.id.role);
            textViewRole.setText(cred.getCast().get(i).getCharacter());

            ImageView imageView = card.findViewById(R.id.thumbnail);

            if (!cred.getCast().get(i).getProfilePath().equals("") && cred.getCast().get(i).getProfilePath() != null) {
                Glide.with(getContext())
                        .load("https://image.tmdb.org/t/p/w500" + cred.getCast().get(i).getProfilePath())
                        .centerCrop()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(imageView);
            }

            int index = i;
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle extras = new Bundle();
                    extras.putSerializable("person",cred.getCast().get(index));

                    FragmentManager manager = getParentFragmentManager();
                    FragmentTransaction fragmentTransaction = manager.beginTransaction();

                    Fragment fragment = new ActorFragment();
                    String tag = "Actor Fragment";

                    fragment.setArguments(extras);
                    fragmentTransaction.replace(R.id.detailsFragmentContainerView, fragment,"Actor Fragment");
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            });

            layout.addView(card);
        }
    }


    private void fillDirector(LinearLayoutCompat layout, Credits cred, String type){

        List<PersonCrew> crew = cred.getCrew();

        for (PersonCrew person : crew){
            if (type.equals("Film")){
                if ((person.getJob().equals("Director") && person.getDepartment().equals("Directing"))){
                    View card = LayoutInflater.from(getActivity()).inflate(R.layout.card_cast_layout, layout, false);

                    TextView textViewTitle = card.findViewById(R.id.title);
                    textViewTitle.setText(person.getName());

                    TextView textViewRole = card.findViewById(R.id.role);
                    textViewRole.setText(person.getJob());

                    ImageView imageView = card.findViewById(R.id.thumbnail);

                    if (!person.getProfilePath().equals("") && person.getProfilePath() != null) {
                        Glide.with(getContext())
                                .load("https://image.tmdb.org/t/p/w500" + person.getProfilePath())
                                .centerCrop()
                                .skipMemoryCache(true)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(imageView);
                    }

                    card.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle extras = new Bundle();
                            extras.putSerializable("person",person);

                            FragmentManager manager = getParentFragmentManager();
                            FragmentTransaction fragmentTransaction = manager.beginTransaction();

                            Fragment fragment = new DirectorFragment();
                            String tag = "Director Fragment";

                            fragment.setArguments(extras);
                            fragmentTransaction.replace(R.id.detailsFragmentContainerView, fragment,"Director Fragment");
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();
                        }
                    });
                    layout.addView(card);
                }
            }else {
                if ((person.getJob().equals("Director") && person.getDepartment().equals("Directing")) || (person.getJob().equals("Executive Producer"))){
                    View card = LayoutInflater.from(getActivity()).inflate(R.layout.card_cast_layout, layout, false);

                    TextView textViewTitle = card.findViewById(R.id.title);
                    textViewTitle.setText(person.getName());

                    TextView textViewRole = card.findViewById(R.id.role);
                    textViewRole.setText(person.getJob());

                    ImageView imageView = card.findViewById(R.id.thumbnail);

                    if (!person.getProfilePath().equals("") && person.getProfilePath() != null) {
                        Glide.with(getContext())
                                .load("https://image.tmdb.org/t/p/w500" + person.getProfilePath())
                                .centerCrop()
                                .skipMemoryCache(true)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(imageView);
                    }

                    card.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle extras = new Bundle();
                            extras.putSerializable("person",person);

                            FragmentManager manager = getParentFragmentManager();
                            FragmentTransaction fragmentTransaction = manager.beginTransaction();

                            Fragment fragment = new DirectorFragment();
                            String tag = "Director Fragment";

                            fragment.setArguments(extras);
                            fragmentTransaction.replace(R.id.detailsFragmentContainerView, fragment,"Director Fragment");
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();
                        }
                    });
                    layout.addView(card);
                }
            }

        }
    }

    private void fillGenre(TextView genreTextView, MovieDb movie){
        genreTextView.append("\n");
        if (movie.getGenres().size() == 0){
            genreTextView.append("Nessun genere trovato");
        }else{
            for(Genre gen : movie.getGenres()){
                genreTextView.append(gen.getName() + " | ");
            }
        }
    }

    private void fillGenre(TextView genreTextView, TvSeries serie){
        genreTextView.append("\n");
        if (serie.getGenres().size() == 0){
            genreTextView.append("Nessun genere trovato");
        }else{
            for(Genre gen : serie.getGenres()){
                genreTextView.append(gen.getName() + " | ");
            }
        }
    }

    private void fillReviews(LinearLayoutCompat reviewDetailsLayout, Object res){
        List<Reviews> reviews = (List<Reviews>) res;

        if (reviews.isEmpty()){
            TextView noRev = new TextView(reviewDetailsLayout.getContext());
            noRev.setLayoutParams(new LinearLayoutCompat.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT));
            noRev.setText("Nessuna recensione trovata");
            noRev.setTextSize(16);
            noRev.setTextColor(Color.parseColor("#979797"));
            noRev.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            reviewDetailsLayout.addView(noRev);
        }else{
            for (int i = 0; i < reviews.size(); i++){
                View card = LayoutInflater.from(getActivity()).inflate(R.layout.review_card_layout, reviewDetailsLayout, false);

                TextView title = card.findViewById(R.id.reviewAuthorDetails);
                title.setText("#" + i);

                TextView body = card.findViewById(R.id.bodyReview);
                body.setText(reviews.get(i).getContent());

                TextView author = card.findViewById(R.id.authorReview);
                author.setText(reviews.get(i).getAuthor());

                reviewDetailsLayout.addView(card);
            }
        }
    }

    private void fillPicassoImg(String path, LottieAnimationView thumbnail){
        if (path != null && !path.equals("")){
            Glide.with(getContext())
                    .load("https://image.tmdb.org/t/p/w342" + path)
                    .centerCrop()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(thumbnail);
        }

    }

    private void fillRaccomandationMovie(LinearLayoutCompat layout, List<MovieDb> res){
        for (int i = 0; i< res.size(); i++){
            if (i == 6){
                break;
            }
            View card = LayoutInflater.from(getActivity()).inflate(R.layout.card_layout, layout, false);

            TextView textViewTitle = card.findViewById(R.id.title);
            textViewTitle.setText(res.get(i).getTitle());

            ImageView imageView = card.findViewById(R.id.thumbnail);

            if (res.get(i).getPosterPath() != null){
                Glide.with(getContext())
                        .load("https://image.tmdb.org/t/p/w342" + res.get(i).getPosterPath())
                        .centerCrop()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(imageView);
            }

            TextView rating = card.findViewById(R.id.rating);
            rating.setText(String.format("%.1f", res.get(i).getVoteAverage()));

            int index = i;
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), DetailsActivity.class);

                    new MoviesApi(new ApiResponse() {
                        @Override
                        public void processFinish(Object output) {
                            MovieDb movie = (MovieDb) output;
                            Bundle extras = new Bundle();
                            extras.putSerializable("Movie", movie);
                            extras.putString("Type", "");
                            intent.putExtras(extras);

                            getContext().startActivity(intent);
                        }
                    }).execute("Get dettagli Film", String.valueOf(res.get(index).getId()));
                }
            });

            layout.addView(card);
        }
    }

    private void fillRaccomandationTv(LinearLayoutCompat layout, List<TvSeries> res){
        for (int i = 0; i< res.size(); i++){
            if (i == 6){
                break;
            }
            View card = LayoutInflater.from(getActivity()).inflate(R.layout.card_layout, layout, false);

            TextView textViewTitle = card.findViewById(R.id.title);
            textViewTitle.setText(res.get(i).getName());

            ImageView imageView = card.findViewById(R.id.thumbnail);

            if (res.get(i).getPosterPath() != null){
                Glide.with(getContext())
                        .load("https://image.tmdb.org/t/p/w342" + res.get(i).getPosterPath())
                        .centerCrop()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(imageView);
            }

            TextView rating = card.findViewById(R.id.rating);
            rating.setText(String.format("%.1f", res.get(i).getVoteAverage()));

            int index = i;
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), DetailsActivity.class);

                    new TvSeriesApi(new ApiResponse() {
                        @Override
                        public void processFinish(Object output) {
                            TvSeries serie = (TvSeries) output;
                            Bundle extras = new Bundle();
                            extras.putSerializable("Tv", serie);
                            extras.putString("Type", "");
                            intent.putExtras(extras);

                            getContext().startActivity(intent);
                        }
                    }).execute("Get dettagli Serie", String.valueOf(res.get(index).getId()));
                }
            });
            layout.addView(card);
        }
    }


    private void fillSimilarMovie(LinearLayoutCompat layout, List<MovieDb> res){
        for (int i = 0; i< res.size(); i++){
            if (i == 6){
                break;
            }
            View card = LayoutInflater.from(getActivity()).inflate(R.layout.card_layout, layout, false);

            TextView textViewTitle = card.findViewById(R.id.title);
            textViewTitle.setText(res.get(i).getTitle());

            ImageView imageView = card.findViewById(R.id.thumbnail);

            if (res.get(i).getPosterPath() != null){
                Glide.with(getContext())
                        .load("https://image.tmdb.org/t/p/w342" + res.get(i).getPosterPath())
                        .centerCrop()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(imageView);
            }

            TextView rating = card.findViewById(R.id.rating);
            rating.setText(String.format("%.1f", res.get(i).getVoteAverage()));

            int index = i;
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), DetailsActivity.class);

                    new MoviesApi(new ApiResponse() {
                        @Override
                        public void processFinish(Object output) {
                            MovieDb movie = (MovieDb) output;
                            Bundle extras = new Bundle();
                            extras.putSerializable("Movie", movie);
                            extras.putString("Type", "");
                            intent.putExtras(extras);

                            getContext().startActivity(intent);
                        }
                    }).execute("Get dettagli Film", String.valueOf(res.get(index).getId()));
                }
            });

            layout.addView(card);
        }
    }

    private void fillSimilarSeries(LinearLayoutCompat layout, List<TvSeries> res){
        List<TvSeries> simListSerie = res;

        for (int i = 0; i<simListSerie.size(); i++){
            if (i == 6){
                break;
            }
            View card = LayoutInflater.from(getActivity()).inflate(R.layout.card_layout, layout, false);

            TextView textViewTitle = card.findViewById(R.id.title);
            textViewTitle.setText(simListSerie.get(i).getName());

            ImageView imageView = card.findViewById(R.id.thumbnail);

            if (simListSerie.get(i).getPosterPath() != null){
                Glide.with(getContext())
                        .load("https://image.tmdb.org/t/p/w342" + simListSerie.get(i).getPosterPath())
                        .centerCrop()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(imageView);
            }

            TextView rating = card.findViewById(R.id.rating);
            rating.setText(String.format("%.1f", simListSerie.get(i).getVoteAverage()));

            int index = i;
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), DetailsActivity.class);

                    new TvSeriesApi(new ApiResponse() {
                        @Override
                        public void processFinish(Object output) {
                            TvSeries tv = (TvSeries) output;
                            Bundle extras = new Bundle();
                            extras.putSerializable("Tv", tv);
                            extras.putString("Type", "");
                            intent.putExtras(extras);

                            getContext().startActivity(intent);
                        }
                    }).execute("Get dettagli Serie", String.valueOf(simListSerie.get(index).getId()));
                }
            });

            layout.addView(card);
        }
    }

    private void fillVideos(LinearLayoutCompat layout, List<Video> videoList){
        if (!videoList.isEmpty()){
            YouTubePlayerView youTubePlayerView = new YouTubePlayerView(getContext());
            youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
                @Override
                public void onReady(YouTubePlayer youTubePlayer) {
                    String videoId = videoList.get(0).getKey();
                    youTubePlayer.cueVideo(videoId,0);
                }
            });

            PlayerUiController controller = youTubePlayerView.getPlayerUiController();
            controller.showVideoTitle(true);
            controller.showDuration(true);
            controller.showFullscreenButton(false);
            controller.showBufferingProgress(true);
            controller.showCurrentTime(true);

            layout.addView(youTubePlayerView);
        }

    }
}
