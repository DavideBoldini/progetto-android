package com.example.justshow;

import android.app.Application;

import com.example.justshow.Items.ProfileItem;

import java.util.ArrayList;
import java.util.List;

public class JustShow extends Application {
    private boolean userLogged = false;
    private ProfileItem activeProfile = null;
    private List<String> lastSearchList = new ArrayList<>();

    public boolean getUserLogged() {
        return  userLogged;
    }

    public void setUserLogged(final boolean newValue){
        this.userLogged = newValue;
    }

    public ProfileItem getActiveProfile(){
        return this.activeProfile;
    }

    public void setActiveProfile(final ProfileItem profile){
        this.activeProfile = profile;
    }

    public List<String> getLastSearchList(){
        return this.lastSearchList;
    }

    public void setLastSearchList(List<String> newSearchList){
        this.lastSearchList = newSearchList;
    }
}
