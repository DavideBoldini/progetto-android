package com.example.justshow.Utilities;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.tv.TvSeries;

public class Converters {


    @TypeConverter
    public static ArrayList<MovieDb> fromJsonMovie(String value){
        Type listType = new TypeToken<ArrayList<MovieDb>>(){}.getType();
        return new Gson().fromJson(value,listType);
    }

    @TypeConverter
    public static ArrayList<TvSeries> fromJsonSeries(String value){
        Type listType = new TypeToken<ArrayList<TvSeries>>(){}.getType();
        return new Gson().fromJson(value,listType);
    }

    @TypeConverter
    public static HashMap<String,String> fromJsonWatchlist(String value){
        Type listType = new TypeToken<HashMap<String,String>>(){}.getType();
        HashMap<String,String> map = new Gson().fromJson(value,listType);
        return map;
    }

    @TypeConverter
    public static String fromMovieList(List<MovieDb> movieList){
        Gson gson = new Gson();
        String json = gson.toJson(movieList);
        return json;
    }
    @TypeConverter
    public static String fromSeriesList(List<TvSeries> tvList){
        Gson gson = new Gson();
        String json = gson.toJson(tvList);
        return json;
    }

    @TypeConverter
    public static String fromWatchlistmap(HashMap<String,String> watchlistMap){
        Gson gson = new Gson();
        String json = gson.toJson(watchlistMap);
        return json;
    }
}
