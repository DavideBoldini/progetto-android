package com.example.justshow.Activity.Home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.justshow.Activity.Details.DetailsActivity;
import com.example.justshow.Activity.Home.News.NewsFragment;
import com.example.justshow.Activity.Home.PopularTvSeries.PopularTvSeriesFragment;
import com.example.justshow.Activity.Home.TopTvSeries.TopTvSeriesFragment;
import com.example.justshow.Activity.Home.UpcomingTvSeries.UpcomingTvSeriesFragment;
import com.example.justshow.R;
import com.example.justshow.Utilities.ApiResponse;
import com.example.justshow.Utilities.TvSeriesApi;
import com.example.justshow.Utilities.TvSeriesUtilities;
import com.kwabenaberko.newsapilib.NewsApiClient;
import com.kwabenaberko.newsapilib.models.Article;
import com.kwabenaberko.newsapilib.models.request.EverythingRequest;
import com.kwabenaberko.newsapilib.models.response.ArticleResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import info.movito.themoviedbapi.model.tv.TvSeries;

public class HomeTvFragment extends Fragment {

    TvSeries serie = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.home_tv_page, container, false);

        Button viewAllUpcoming = view.findViewById(R.id.allUpcomingTvButton);
        viewAllUpcoming.setOnClickListener(v -> {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mainLayout, new UpcomingTvSeriesFragment(),"UpcomingTvSeriesFragment");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        Button viewAllPopular = view.findViewById(R.id.allPopularTvButton);
        viewAllPopular.setOnClickListener(v -> {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mainLayout, new PopularTvSeriesFragment(),"PopularTvSeriesFragment");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        Button viewAllTop = view.findViewById(R.id.allTopRatedTvButton);
        viewAllTop.setOnClickListener(v -> {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mainLayout, new TopTvSeriesFragment(),"TopTvSeriesFragment");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        Button viewAllNews = view.findViewById(R.id.allTvNewsButton);
        viewAllNews.setOnClickListener(v -> {
            Bundle extras = new Bundle();
            extras.putString("Type", "Tv Series");

            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mainLayout, new NewsFragment(extras),"NewsFragment");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        LinearLayoutCompat horizontalPopularTvLayout = view.findViewById(R.id.horizontalPopularTvLayout);
        LinearLayoutCompat horizontalUpcomingTvLayout = view.findViewById(R.id.horizontalUpcomingTvLayout);
        LinearLayoutCompat horizontalTopRatedTvLayout = view.findViewById(R.id.horizontalTopRatedTvLayout);

        LayoutInflater inflater = LayoutInflater.from(getActivity());

        fillHorizontalUpcomingLayout(horizontalUpcomingTvLayout,inflater);
        fillHorizontalPopularLayout(horizontalPopularTvLayout,inflater);
        fillHorizontalTopRatedLayout(horizontalTopRatedTvLayout,inflater);

        LinearLayoutCompat horizontalNewsTvCardLayout = view.findViewById(R.id.horizontalTvNewsCardLayout);
        fillHorizontalNewsMovieCardLayout(horizontalNewsTvCardLayout,inflater);

    }



    private void fillHorizontalNewsMovieCardLayout(LinearLayoutCompat layout, LayoutInflater inflater){

        NewsApiClient newsApiClient = new NewsApiClient("cda09fbffad74cd5a1852e20b81ba8ee");
        newsApiClient.getEverything(
                new EverythingRequest.Builder()
                        .q("Tv Series")
                        .build(),
                new NewsApiClient.ArticlesResponseCallback() {
                    @Override
                    public void onSuccess(ArticleResponse response) {
                        fillNewsLayout(layout,inflater,response);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        System.out.println(throwable.getMessage());
                    }
                }
        );
    }

    private void fillHorizontalUpcomingLayout(LinearLayoutCompat layout, LayoutInflater inflater) {
        new TvSeriesApi(new ApiResponse() {
            @Override
            public void processFinish(Object output) {
                TvSeriesUtilities.upcomingTv = (List<TvSeries>) output;
                fillLayout(layout,inflater,output,"Upcoming");
            }
        }).execute("UpcomingTv");
    }

    private void fillHorizontalPopularLayout(LinearLayoutCompat layout, LayoutInflater inflater){
        new TvSeriesApi(new ApiResponse() {
            @Override
            public void processFinish(Object output) {
                TvSeriesUtilities.popTv = (List<TvSeries>) output;
                fillLayout(layout,inflater,output,"Popular");
            }
        }).execute("PopTv");
    }

    private void fillHorizontalTopRatedLayout(LinearLayoutCompat layout, LayoutInflater inflater){
        new TvSeriesApi(new ApiResponse() {
            @Override
            public void processFinish(Object output) {
                TvSeriesUtilities.topTv = (List<TvSeries>) output;
                fillLayout(layout,inflater,output,"Top");
            }
        }).execute("TopTv");
    }

    private void fillLayout(LinearLayoutCompat layout, LayoutInflater inflater, Object res, String type){
        List<TvSeries> tvRes = (List<TvSeries>) res;
        for (int i = 0; i < 5;  i++){
            View card = inflater.inflate(R.layout.card_layout, layout, false);

            TextView textView = card.findViewById(R.id.title);
            textView.setText(tvRes.get(i).getName());

            ImageView imageView = card.findViewById(R.id.thumbnail);

            if (tvRes.get(i).getPosterPath() != null){
                Glide.with(getContext())
                        .load("https://image.tmdb.org/t/p/original" + tvRes.get(i).getPosterPath())
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }

            TextView rating = card.findViewById(R.id.rating);
            rating.setText(String.format("%.1f", tvRes.get(i).getVoteAverage()));

            final int index2 = i;
            card.setOnClickListener(v -> {
                Intent intent = new Intent(getContext(), DetailsActivity.class);

                new TvSeriesApi(new ApiResponse() {
                    @Override
                    public void processFinish(Object output) {
                        serie = (TvSeries) output;
                        Bundle extras = new Bundle();
                        extras.putSerializable("Tv", serie);
                        extras.putString("Type", type);
                        intent.putExtras(extras);

                        getContext().startActivity(intent);
                    }
                }).execute("Get dettagli Serie", String.valueOf(tvRes.get(index2).getId()));
            });
            layout.addView(card);
        }
    }


    private void fillNewsLayout(LinearLayoutCompat layout, LayoutInflater inflater, ArticleResponse resIn){
        List<Article> articleList = resIn.getArticles();

        for(int i = 0; i < 5;  i++){
            View card = inflater.inflate(R.layout.news_card_layout, layout, false);

            TextView textView = card.findViewById(R.id.title);
            textView.setText(articleList.get(i).getTitle());

            ImageView imageView = card.findViewById(R.id.thumbnail);

            Picasso.get()
                    .load(articleList.get(i).getUrlToImage())
                    .resize(300,150)
                    .centerCrop()
                    .into(imageView);

            TextView description = card.findViewById(R.id.description);
            TextView source = card.findViewById(R.id.source);

            description.setText(articleList.get(i).getDescription());
            source.setText(articleList.get(i).getSource().getName());

            final int index = i;
            card.setOnClickListener(v -> {
                Uri uri = Uri.parse(articleList.get(index).getUrl());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            });

            layout.addView(card);
        }

    }

}