package com.example.justshow.Activity.Login;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.justshow.Database.ProfileRepository;
import com.example.justshow.Items.ProfileItem;

import java.util.List;

public class LoginViewModel extends AndroidViewModel {

    private LiveData<List<ProfileItem>> profileItems;
    private final ProfileRepository repository;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        repository = new ProfileRepository(application);
    }

    public LiveData<List<ProfileItem>> getProfileByEmail(String email){
        profileItems = repository.getProfileByEmail(email);
        return profileItems;
    }

    public LiveData<List<ProfileItem>> checkLogin(String email,String password){
        profileItems = repository.checkLogin(email,password);
        return profileItems;
    }
}
