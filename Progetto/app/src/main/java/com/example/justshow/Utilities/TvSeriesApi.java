package com.example.justshow.Utilities;

import android.os.AsyncTask;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.TmdbReviews;
import info.movito.themoviedbapi.TmdbSearch;
import info.movito.themoviedbapi.TmdbTV;
import info.movito.themoviedbapi.TmdbTvSeasons;
import info.movito.themoviedbapi.TvResultsPage;
import info.movito.themoviedbapi.model.Discover;
import info.movito.themoviedbapi.model.Genre;
import info.movito.themoviedbapi.model.MovieImages;
import info.movito.themoviedbapi.model.Reviews;

import info.movito.themoviedbapi.model.config.Timezone;
import info.movito.themoviedbapi.model.tv.TvSeason;
import info.movito.themoviedbapi.model.tv.TvSeries;

public class TvSeriesApi extends AsyncTask<String, Void, Object> {

    TmdbTV series;
    TmdbSearch search;

    public ApiResponse delegate;

    public TvSeriesApi(){}
    public TvSeriesApi(ApiResponse delegate){
        this.delegate = delegate;
    }

    @Override
    protected Object doInBackground(String... strings) {
        TmdbApi tmdb = new TmdbApi("db981d35add49fc538c2afdd56fd00cf");
        search = tmdb.getSearch();
        series = tmdb.getTvSeries();
        switch (strings[0]){
            case "PopTv":
                return this.getPopularTvSeries();
            case "TopTv":
                return this.getTopRatedTvSeries();
            case "UpcomingTv":
                return this.getUpcomingTvSeries();
            case "Cerca Serie":
                return this.searchSerieList(strings[1]);
            case "Get dettagli Serie":
                return this.getDettagliSerie(strings[1]);
            case "Get dettagli Serie by Title":
                return this.getDettagliSerieByTitle(strings[1]);
            case "Get Immagini Serie":
                return this.getSerieImages(strings[1]);
            case "Get Review Serie":
                return this.getReviewSerie(tmdb,strings[1]);
            case "Get Season Serie By Num":
                return this.getSeasonSerieByNum(tmdb,strings[1],Integer.parseInt(strings[2]));
            case "Cerca Serie dettagliata":
                return this.getSerieDeepSearch(tmdb,strings[1],strings[2],strings[3],strings[4],strings[5],strings[6],strings[7],strings[8],strings[9],strings[10],strings[11],strings[12],strings[13], strings[14],
                strings[15], strings[16]);
            case "Get Similar Serie":
                return this.getSimilarSerie(tmdb,strings[1]);
        }
        return Collections.emptyList();
    }

    @Override
    protected void onPostExecute(Object o) {
        if (delegate != null){
            delegate.processFinish(o);
        }
    }


    private List<TvSeries> getSerieDeepSearch(TmdbApi tmdb, String titleIn, String genre1, String genre2, String genre3, String genre4,
                                              String genre5, String genre6, String genre7, String genre8, String genre9, String genre10, String genre11, String minVote, String maxVote, String daYear, String aYear) {

        List<TvSeries> resList;

        List<String> genreList = new ArrayList<>();
        genreList.add(genre1);
        genreList.add(genre2);
        genreList.add(genre3);
        genreList.add(genre4);
        genreList.add(genre5);
        genreList.add(genre6);
        genreList.add(genre7);
        genreList.add(genre8);
        genreList.add(genre9);
        genreList.add(genre10);
        genreList.add(genre11);

        genreList.removeAll(Collections.singleton(null));

        if (titleIn != null){
            resList = new ArrayList<>();
            ArrayList<TvSeries> resListSearch = new ArrayList<>(this.searchSerieList(titleIn));
            for(TvSeries res : resListSearch){
                TvSeries serie = this.getDettagliSerie(String.valueOf(res.getId()));
                if (serie.getVoteAverage() >= Float.parseFloat(minVote) && serie.getVoteAverage() <= Float.parseFloat(maxVote)){
                    if (serie.getFirstAirDate() != null && !serie.getFirstAirDate().equals("")){
                        String[] splits = serie.getFirstAirDate().split("-");
                        int year = Integer.parseInt(splits[0]);
                        if(year >= Integer.parseInt(daYear) && year <= Integer.parseInt(aYear)){
                            if (!genreList.isEmpty()){
                                for (Genre genre : serie.getGenres()) {
                                    if (genreList.contains(String.valueOf(genre.getId()))){
                                        resList.add(serie);
                                        break;
                                    }
                                }
                            }else {
                                resList.add(serie);
                            }

                        }
                    }
                }
            }
        }else{
            TvApiDiscover discoverTmdb = new TvApiDiscover(tmdb);
            Discover discover = new Discover();
            discover.voteAverageGte(Float.parseFloat(minVote));
            discover.releaseDateGte(daYear +"-01-01");
            discover.releaseDateLte(aYear+"-12-31");
            discover.includeAdult(false);
            discover.language("it-IT");
            if(genreList.size() > 1){
                discover.withGenres(StringUtils.join(genreList,"|"));
            }else if (genreList.size() == 1){
                discover.withGenres(genreList.get(0));
            }

            resList = new ArrayList<>();
            TvResultsPage resPage = discoverTmdb.getDiscoverSeries(discover);
            for (int i = 2; i<= resPage.getTotalPages()+1; i++){
                if (resList.size() >= 500){
                    break;
                }
                discover.page(i-1);
                resPage = discoverTmdb.getDiscoverSeries(discover);
                resList.addAll(resPage.getResults());
            }

            Iterator<TvSeries> iter = resList.iterator();
            while(iter.hasNext()){
                TvSeries tv = iter.next();
                if (tv.getVoteAverage() > Float.parseFloat(maxVote)){
                    iter.remove();
                }

                if (tv.getFirstAirDate() != null && !tv.getFirstAirDate().equals("")){
                    String[] splits = tv.getFirstAirDate().split("-");
                    int year = Integer.parseInt(splits[0]);
                    if (year > Integer.parseInt(aYear) || year < Integer.parseInt(daYear)){
                        iter.remove();
                    }
                }
            }

        }
        return resList;
    }

    private TvSeason getSeasonSerieByNum(TmdbApi tmdb, String serieName, int seasonInt) {
        TvResultsPage resPage = search.searchTv(serieName,"it-IT",0);

        TmdbTvSeasons tvSeason = tmdb.getTvSeasons();
        TvSeason res = null;
        for(TvSeries elem : resPage.getResults()){
            if(elem.getName().equals(serieName)){
                res = tvSeason.getSeason(elem.getId(),seasonInt,"it-IT");
                return res;
            }
        }
        return res;
    }


    private List<Reviews> getReviewSerie(TmdbApi tmdb, String serieName) {
        TvResultsPage resPage = search.searchTv(serieName,"it-IT",0);

        TvSeries res = null;
        List<Reviews> resRev;
        for(TvSeries elem : resPage.getResults()){
            if(elem.getName().equals(serieName)){
                res = series.getSeries(elem.getId(),"it-IT");
                break;
            }
        }
        TmdbReviews reviews = tmdb.getReviews();

        try {
            resRev =  reviews.getReviews(res.getId(),null,1).getResults();
        }catch (Exception e){
            resRev = new ArrayList<>();
        }
        return resRev;

    }

    private MovieImages getSerieImages(String serieName) {
        TvResultsPage resPage = search.searchTv(serieName,"it-IT",0);

        MovieImages res = null;
        for(TvSeries elem : resPage.getResults()){
            if(elem.getName().equals(serieName)){
                res = series.getImages(elem.getId(),null);
                return res;
            }
        }
        return res;
    }

    private TvSeries getDettagliSerie(String idSerie) {
        int id = Integer.parseInt(idSerie);
        TvSeries res = series.getSeries(id,"it-IT", TmdbTV.TvMethod.videos, TmdbTV.TvMethod.recommendations, TmdbTV.TvMethod.credits);
        return res;
    }

    private TvSeries getDettagliSerieByTitle(String title) {
        TvSeries res = null;
        TvResultsPage resPage = search.searchTv(title,"it-IT",0);
        for(TvSeries tv : resPage.getResults()){
            if (tv.getName().equals(title)){
                res = series.getSeries(tv.getId(),"it-IT", TmdbTV.TvMethod.videos, TmdbTV.TvMethod.recommendations, TmdbTV.TvMethod.credits);
                return res;
            }
        }
        return res;
    }

    private List<TvSeries> getSimilarSerie(TmdbApi tmdb, String idSerie){
        TvApiDiscover discoverTmdb = new TvApiDiscover(tmdb);
        TvResultsPage res = discoverTmdb.getSimilarSerie(Integer.parseInt(idSerie),"it-IT",0);
        List<TvSeries> resList = res.getResults();
        return  resList;
    }

    private List<TvSeries> searchSerieList(String serieName) {
        TvResultsPage resPage = search.searchTv(serieName,"it-IT",0);
        return resPage.getResults();
    }


    public List<TvSeries> getTopRatedTvSeries(){
        TvResultsPage resPage = series.getTopRated("it-IT",0);
        return resPage.getResults();
    }

    public List<TvSeries> getPopularTvSeries(){
        TvResultsPage resPage = series.getPopular("it-IT",0);
        return resPage.getResults();
    }


    public List<TvSeries> getUpcomingTvSeries(){
        TvResultsPage resPage = series.getAiringToday("it-IT",0,new Timezone("Europe/Dublin","Italy"));
        return resPage.getResults();
    }
}
